import { IPrettyDiffResult, PrettyDiff } from '../src';
import { removeWhitespace } from '../src/util';

const fs = require('fs');
const path = require('path');

describe('PrettyDiff', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff({ ignoreWhitespace: false });
  });

  it('simple insert tags', () => {
    let oldContent = '<p>asd asd qwe zxc asd aqwe dfsdf </p>';
    let newContent =
      '<p>asd asd qwe <img src="bla.jpg"/> zxc asd aqwe dfsdf </p>';

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent:
        '<p>asd asd qwe <ins><img src="bla.jpg"/> </ins>zxc asd aqwe dfsdf </p>',
      prevContent: '<p>asd asd qwe <del></del>zxc asd aqwe dfsdf </p>',
    };
    expect(result).toEqual(expected);
  });

  it('simple delete tags', () => {
    let oldContent =
      '<p>asd asd qwe <img src="bla.jpg"/> zxc asd aqwe dfsdf </p>';
    let newContent = '<p>asd asd qwe zxc asd aqwe dfsdf </p>';

    let result = instance.process(oldContent, newContent);

    let expected = {
      prevContent:
        '<p>asd asd qwe <del><img src="bla.jpg"/> </del>zxc asd aqwe dfsdf </p>',
      newContent: '<p>asd asd qwe <del></del>zxc asd aqwe dfsdf </p>',
    };
    expect(result).toEqual(expected);
  });

  it('simple replace tags', () => {
    let oldContent =
      '<p>asd asd qwe <img src="bla.jpg"/> zxc asd aqwe dfsdf </p>';
    let newContent = '<p>asd asd qwe <br/>zxc asd aqwe dfsdf </p>';

    let result = instance.process(oldContent, newContent);

    let expected = {
      prevContent:
        '<p>asd asd qwe <repl><img src="bla.jpg"/> </repl>zxc asd aqwe dfsdf </p>',
      newContent: '<p>asd asd qwe <repl><br/></repl>zxc asd aqwe dfsdf </p>',
    };
    expect(result).toEqual(expected);
  });

  it('matches whole area', () => {
    let oldContent = `<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<span style="display: inline-block; border: 1px solid #b0b0e2; padding: 0.03in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></p>
</ul>`;
    let newContent = `<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<span style="display: inline-block; border: 1px solid #b0b0e2; padding: 2in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></p>
</ul>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      prevContent: `<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<repl><span style="display: inline-block; border: 1px solid #b0b0e2; padding: 0.03in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></repl></p>
</ul>`,
      newContent: `<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<repl><span style="display: inline-block; border: 1px solid #b0b0e2; padding: 2in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></repl></p>
</ul>`,
    };
    expect(result.prevContent).toEqual(expected.prevContent);
    expect(result.newContent).toEqual(expected.newContent);
    // expect(result).toEqual(expected);
  });

  it('buggy 1', () => {
    const prevContent = `<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
  <font color="#222222"><font face="Arial, serif"><font size="3" style="font-size: 12pt">длинный текст<br/>
    <br/>
  </font></font></font><br/>

</p>`;
    const newContent = `<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
  <font color="#222222"><font face="Arial, serif"><font size="3" style="font-size: 12pt; font-weight: 100"><b>длин<i>ный</b> текст</i><br/>
    <br/>
  </font></font></font><br/>

</p>`;

    const expected = {
      prevContent: `<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
  <font color="#222222"><font face="Arial, serif"><del><font size="3" style="font-size: 12pt">длинный текст<br/>
    <br/>
  </font></del></font></font><br/>

</p>`,
      newContent: `<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
  <font color="#222222"><font face="Arial, serif"><ins><font size="3" style="font-size: 12pt; font-weight: 100"><b>длин<i>ный</b> текст</i><br/>
    <br/>
  </font></ins></font></font><br/>

</p>`,
    };

    const actual = instance.process(prevContent, newContent);

    expect(actual.prevContent).toEqual(expected.prevContent);
    expect(actual.newContent).toEqual(expected.newContent);
  });

  it('buggy case 2', () => {
    // language=HTML format=false
    const prevContent = `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<font color="#222222">
<font face="Arial, serif">
<font size="3" style="font-size: 12pt">
длинный текст
<br/>
<br/>
</font>
</font>
</font>
<br/>
</p>
`;
    // language=HTML format=false
    const newContent = `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<img src="asdasd"/>
<font color="#222222">
<font face="Arial, serif">
<font size="3" style="font-size: 12pt; font-weight: 100">
<b>длин<i>ный</b>текст</i>
<br/>
<br/>
</font>
</font>
</font>
<br/>
</p>
`;
    // language=HTML format=false
    const expectedPrev = `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<del></del><font color="#222222">
<font face="Arial, serif">
<repl><font size="3" style="font-size: 12pt">
длинный текст
<br/>
<br/>
</font></repl>
</font>
</font>
<br/>
</p>
`;
    // language=HTML format=false
    const expectedNew = `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<ins><img src="asdasd"/>
</ins><font color="#222222">
<font face="Arial, serif">
<repl><font size="3" style="font-size: 12pt; font-weight: 100">
<b>длин<i>ный</b>текст</i>
<br/>
<br/>
</font></repl>
</font>
</font>
<br/>
</p>
`;

    const actual = instance.process(prevContent, newContent);

    expect(actual.newContent).toEqual(expectedNew);
    expect(actual.prevContent).toEqual(expectedPrev);
  });

  it('buggy case 3', () => {
    // language=HTML format=false
    const prevContent = `
<p class="western" style="margin-bottom: 0in; line-height: 100%"> <br/> </p> 
  <table width="136" cellpadding="2" cellspacing="1" style="page-break-before: always"> 
   <colgroup>
    <col width="32"> 
    <col width="41"> 
    <col width="5"> 
    <col width="5"> 
    <col width="5"> 
    <col width="5"> 
    <col width="6"> 
   </colgroup>
   <tbody>
    <tr> 
     <td width="32" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0">еуке</p> </td> 
     <td width="41" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> укеуу</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> уе</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
   </tbody>
  </table> 
<p class="western" align="left" style="line-height: 100%; orphans: 0; widows: 0"> sdfsdfsdfsdfsd</p>  
`;
    // language=HTML format=false
    const newContent = `
<p class="western" style="margin-bottom: 0in; line-height: 100%"> &nbsp;</p> 
  <table width="136" cellpadding="2" cellspacing="1" style="page-break-before: always"> 
   <colgroup>
    <col width="32"> 
    <col width="41"> 
    <col width="5"> 
    <col width="5"> 
    <col width="5"> 
    <col width="5"> 
    <col width="6"> 
   </colgroup>
   <tbody>
    <tr> 
     <td width="32" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> еуке</p> </td> 
     <td width="41" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> укеуу</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border: 1px double #808080; padding: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> уе</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
    <tr> 
     <td width="32" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="41" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="5" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
     <td width="6" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.02in; padding-right: 0.02in"><p class="western" align="left" style="orphans: 0; widows: 0"> &nbsp;</p> </td> 
    </tr> 
   </tbody>
  </table> 
<p class="western" align="left" style="line-height: 100%; orphans: 0; widows: 0"> sdfsdfsdfsdfsdsdfsdfsdfsd</p> 
<p class="western" align="left" style="line-height: 100%; orphans: 0; widows: 0"> fasdfasfasda</p>  
`;
    // language=HTML format=false
    const expectedPrev = `

`;
    // language=HTML format=false
    const expectedNew = `

`;

    const actual = instance.process(prevContent, newContent);

    // expect(actual.newContent).toEqual(expectedNew);
    expect(actual.newContent).toEqual(newContent);
    expect(actual.prevContent).toEqual(expectedPrev);
  });

  it('kinda real file test 1', () => {
    const prevFileName = 'v1';
    const newFileName = 'v2';

    const prevFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${prevFileName}.html`),
      { encoding: 'utf8' },
    );
    const newFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${newFileName}.html`),
      { encoding: 'utf8' },
    );

    const prevFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${prevFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );
    const newFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${newFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );

    let result = instance.process(prevFileContent, newFileContent);

    // save results to file
    fs.mkdirSync(path.join(__dirname, '../__tests-output__'), {
      recursive: true,
    });
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${prevFileName}-actual.html`,
      ),
      result.prevContent,
      { encoding: 'utf8' },
    );
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${newFileName}-actual.html`,
      ),
      result.newContent,
      { encoding: 'utf8' },
    );

    const expected: IPrettyDiffResult = {
      prevContent: prevFileExpectedContent,
      newContent: newFileExpectedContent,
    };

    // const expected = {};
    // expect(result.prevContent.replace(/\s\s+/, ' ')).toEqual(expected.prevContent.replace(/\s\s+/, ' '));
    expect(result.prevContent).toEqual(expected.prevContent);
    // expect(result.newContent.replace(/\s\s+/, ' ')).toEqual(expected.newContent.replace(/\s\s+/, ' '));
    expect(result.newContent).toEqual(expected.newContent);
  });

  it('kinda real file test 2', () => {
    const prevFileName = 'v3';
    const newFileName = 'v4';

    const prevFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${prevFileName}.html`),
      { encoding: 'utf8' },
    );
    const newFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${newFileName}.html`),
      { encoding: 'utf8' },
    );

    const prevFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${prevFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );
    const newFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${newFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );

    let result = instance.process(prevFileContent, newFileContent);

    // save results to file
    fs.mkdirSync(path.join(__dirname, `../__tests-output__/`), {
      recursive: true,
    });
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${prevFileName}-actual.html`,
      ),
      result.prevContent,
      { encoding: 'utf8' },
    );
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${newFileName}-actual.html`,
      ),
      result.newContent,
      { encoding: 'utf8' },
    );

    const expected: IPrettyDiffResult = {
      prevContent: prevFileExpectedContent,
      newContent: newFileExpectedContent,
    };

    // const expected = {};
    // expect(result.prevContent.replace(/\s\s+/, ' ')).toEqual(expected.prevContent.replace(/\s\s+/, ' '));
    expect(result.prevContent).toEqual(expected.prevContent);
    // expect(result.newContent.replace(/\s\s+/, ' ')).toEqual(expected.newContent.replace(/\s\s+/, ' '));
    expect(result.newContent).toEqual(expected.newContent);
  });

  it('real file test 3', () => {
    const prevFileName = 'v5';
    const newFileName = 'v6';

    const prevFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${prevFileName}.html`),
      { encoding: 'utf8' },
    );
    const newFileContent = fs.readFileSync(
      path.join(__dirname, `../__tests-data__/${newFileName}.html`),
      { encoding: 'utf8' },
    );

    const prevFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${prevFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );
    const newFileExpectedContent = fs.readFileSync(
      path.join(
        __dirname,
        `../__tests-data__/${prevFileName}-to-${newFileName}--${newFileName}-expected.html`,
      ),
      { encoding: 'utf8' },
    );

    let result = instance.process(prevFileContent, newFileContent);

    // save results to file
    fs.mkdirSync(path.join(__dirname, `../__tests-output__/`), {
      recursive: true,
    });
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${prevFileName}-actual.html`,
      ),
      result.prevContent,
      { encoding: 'utf8' },
    );
    fs.writeFileSync(
      path.join(
        __dirname,
        `../__tests-output__/${prevFileName}-to-${newFileName}--${newFileName}-actual.html`,
      ),
      result.newContent,
      { encoding: 'utf8' },
    );

    const expected: IPrettyDiffResult = {
      prevContent: prevFileExpectedContent,
      newContent: newFileExpectedContent,
    };

    // const expected = {};
    // expect(result.prevContent.replace(/\s\s+/, ' ')).toEqual(expected.prevContent.replace(/\s\s+/, ' '));
    expect(result.prevContent).toEqual(expected.prevContent);
    // expect(result.newContent.replace(/\s\s+/, ' ')).toEqual(expected.newContent.replace(/\s\s+/, ' '));
    expect(result.newContent).toEqual(expected.newContent);
  });
});
