import { PrettyDiff } from '../src';

describe('PrettyDiff Table tests', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff({ ignoreWhitespace: false });
  });
  it.skip('marks row on cell content change', () => {
    let oldContent = `<p>bla bla bla</p>
<table>
<tr><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr><td>1</td><td>2insert</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent:
        '<p>bla bla bla</p>\n<table>\n<tr replace><td>1</td><td>2insert</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
      prevContent:
        '<p>bla bla bla</p>\n<table>\n<tr replace><td>1</td><td>2</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
    };
    expect(result).toEqual(expected);
  });

  it.skip('marks row on cell attribute change', () => {
    let oldContent = `<p>bla bla bla</p>
<table>
<tr><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr><td>1</td><td style='background:red'>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent:
        '<p>bla bla bla</p>\n<table>\n<tr replace><td>1</td><td style=\'background:red\'>2</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
      prevContent:
        '<p>bla bla bla</p>\n<table>\n<tr replace><td>1</td><td>2</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
    };
    expect(result).toEqual(expected);
  });

  it.skip('marks table on row attribute change', () => {
    let oldContent = `<p>bla bla bla</p>
<table>
<tr><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent: '<p>bla bla bla</p>\n<table replace>\n<tr some-attribute><td>1</td><td>2</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
      prevContent: '<p>bla bla bla</p>\n<table replace>\n<tr><td>1</td><td>2</td></tr>\n<tr><td>3</td><td>4</td></tr>\n</table>',
    };
    expect(result).toEqual(expected);
  });
  it('puts table into insert tags on insert and places del tags', () => {
    let oldContent = `<p>bla bla bla</p>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent: `<p>bla bla bla</p><ins>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table></ins>`,
      prevContent: `<p>bla bla bla</p><del></del>`,
    };
    expect(result).toEqual(expected);
  });
  it.skip('marks table as replace on row delete', () => {
    let oldContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
<tr><td>5</td><td>6</td></tr>
</table>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent: `<p>bla bla bla</p>
<table replace>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`,
      prevContent: `<p>bla bla bla</p>
<table replace>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
<tr><td>5</td><td>6</td></tr>
</table>`,
    };
    expect(result).toEqual(expected);
  });
  it.skip('marks table as replace on row insert', () => {
    let oldContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table>`;
    let newContent = `<p>bla bla bla</p>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
<tr><td>5</td><td>6</td></tr>
</table>`;

    let result = instance.process(oldContent, newContent);

    let expected = {
      newContent: `<p>bla bla bla</p><ins>
<table>
<tr some-attribute><td>1</td><td>2</td></tr>
<tr><td>3</td><td>4</td></tr>
</table></ins>`,
      prevContent: `<p>bla bla bla</p><del></del>`,
    };
    expect(result).toEqual(expected);
  });
});
