import { PrettyDiff } from '../src';
import {
  DiffOperation,
  DiffResultElement,
  performDiff,
  performDiffCompact,
} from '../src/jsStringDiff';

describe.skip('findAffectedArea', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff();
  });

  it('area for text without tags DIFF INSERT', () => {
    const prev = `<p>asd<span class="asd">value</span><span>another</span></p>`;
    const next = `<p>asd<span class="asd">value and some new text</span><span>another</span></p>`;
    // const diffResult = performDiffCompact(next,prev);
    const diffResult = [
      {
        content: ' and some new text',
        length: 18,
        operation: DiffOperation.INSERT,
        start: 29,
        reference: 29,
      },
    ];
    // expect(diffResult).toEqual([]);

    let result = instance.findAffectedArea(<DiffResultElement>diffResult[0]);
    expect(result.newEnd).toEqual(47);
    expect(result.newStart).toEqual(29);
    expect(result.oldEnd).toEqual(29);
    expect(result.oldStart).toEqual(29);
  });

  it('area for text without tags DIFF DELETE', () => {
    const prev = `<p>asd<span class="asd">value</span><span>another</span></p>`;
    const next = `<p>asd<span class="asd"></span><span>another</span></p>`;
    // const diffResult = performDiffCompact(next,prev);
    const diffResult = [
      {
        content: 'value',
        length: 5,
        operation: 'delete',
        start: 24,
        reference: 24,
      },
    ];
    // expect(diffResult).toEqual([]);

    let result = instance.findAffectedArea(<DiffResultElement>diffResult[0]);
    expect(result.newEnd).toEqual(24);
    expect(result.newStart).toEqual(24);
    expect(result.oldEnd).toEqual(29);
    expect(result.oldStart).toEqual(24);
  });
  it('area for text without tags DIFF REPLACE', () => {
    const prev = `<p>asd<span class="asd">value</span><span>another</span></p>`;
    const next = `<p>asd<span class="asd">123456789</span><span>another</span></p>`;
    // const diffResult = performDiffCompact(next,prev);
    const diffResult = [
      {
        content: 'value',
        length: 5,
        operation: DiffOperation.DELETE,
        start: 24,
        reference: 24,
      },
    ];
    // expect(diffResult).toEqual([]);

    let result = instance.findAffectedArea(<DiffResultElement>diffResult[0]);
    expect(result.newEnd).toEqual(24);
    expect(result.newStart).toEqual(24);
    expect(result.oldEnd).toEqual(29);
    expect(result.oldStart).toEqual(24);
  });

  it('area change inside SELFCLOSING tag', () => {
    const prev = `<p>asd<img src="some.jpg"/><span>another</span></p>`;
    const next = `<p>asd<br/><span>another</span></p>`;
    // const diffResult = performDiff(next,prev);
    const diffResult = [
      {
        content: 'img s',
        length: 5,
        operation: 'delete',
        reference: 7,
        start: 7,
      },
      {
        content: 'b',
        length: 1,
        operation: 'insert',
        reference: 12,
        start: 7,
      },
      {
        content: 'c="some.jpg"',
        length: 12,
        operation: 'delete',
        reference: 13,
        start: 9,
      },
    ];
    // expect(diffResult).toEqual([]);

    instance.filePreviousContents = prev;
    instance.fileNewContents = next;
    let result = instance.findAffectedArea(<DiffResultElement>diffResult[0]);
    expect(result.newEnd).toEqual(11);
    expect(result.newStart).toEqual(6);
    expect(result.oldEnd).toEqual(27);
    expect(result.oldStart).toEqual(6);
  });
});
