import {IRange, PrettyDiff} from '../src';
import {TagRegexResult} from '../src/TagRegexResult';

describe('findCompleteTagRange', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff();
  });

  it('extend simple case', () => {
    let content = 'sad <b>asd <p>da da</b> d da</p> sd asd sa dads';
    // findCompleteTagRange
    let initialRange: IRange = {start: 11, end: 22};
    let tags = instance
      .findTagsInString(content)
      .map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
    let result = instance.findCompleteTagRange(tags, initialRange);

    let expected: IRange & {leftShift:number} = {start: 4, end: 32, leftShift: 1};

    expect(result).toEqual(expected);
  });

  it('extend complex case', () => {
    let content = `<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<span style="display: inline-block; border: 1px solid #b0b0e2; padding: 2in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></p>
</ul>`;
    // findCompleteTagRange
    let initialRange: IRange = {start: 181, end: 366};
    let tags = instance
      .findTagsInString(content)
      .map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
    let result = instance.findCompleteTagRange(tags, initialRange);

    let expected: IRange & {leftShift:number} = {start: 104, end: 394, leftShift: 1};
    console.log(content.slice(result.start, result.end));
    expect(result).toEqual(expected);
  });
});
