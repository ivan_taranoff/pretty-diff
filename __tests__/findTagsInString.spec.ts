import { PrettyDiff } from '../src';
import { DiffOperation, DiffResultElement } from '../src/jsStringDiff';

describe('processInsideOfTagCase', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff();
  });

  it('finds all tags', () => {
    let content = `<a href=""><span></span > b< b></ a> < /   b> < img  /  > <img/>`;

    let result = instance.findTagsInString(content);

    expect(result.length).toBe(8);
  });

  it('finds all specific tags', () => {
    let content = `<a href=""><span></span> b< b></ a> < /   b> < img  /  > <img/>`;

    let result = instance.findTagsInString(content, 'b');
    expect(result.length).toBe(2);

    let tagStrings = result.map(e => e[0]);
    expect(tagStrings).toEqual(['< b>', '< /   b>']);

    result = instance.findTagsInString(content, 'a');
    expect(result.length).toBe(2);
    result = instance.findTagsInString(content, 'span');
    expect(result.length).toBe(2);
    result = instance.findTagsInString(content, 'img');
    expect(result.length).toBe(2);
  });
});
