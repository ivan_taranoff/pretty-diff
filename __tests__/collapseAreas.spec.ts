import { IAffectedArea, PrettyDiff } from '../src';
import { DiffOperation } from '../src/jsStringDiff';

describe('collapseAreas', () => {
  let instance: PrettyDiff;
  beforeEach(() => {
    instance = new PrettyDiff();
  });

  it('bypasses single length array', () => {
    const singleValue: IAffectedArea[] = [
      {
        operation: DiffOperation.INSERT,
        oldStart: 0,
        oldEnd: 1,
        newStart: 10,
        newEnd: 10,
        diffArr: [],
      },
    ];

    expect(instance.collapseAreas(singleValue)).toEqual(singleValue);
  });

  it('should collapse into single value', () => {
    const areas: IAffectedArea[] = [
      {
        operation: DiffOperation.INSERT,
        oldStart: 7,
        oldEnd: 7,
        newStart: 9,
        newEnd: 9,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 5,
        oldEnd: 7,
        newStart: 8,
        newEnd: 9,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 5,
        oldEnd: 9,
        newStart: 6,
        newEnd: 10,
        diffArr: [],
      },
      {
        operation: DiffOperation.REPLACE,
        oldStart: 6,
        oldEnd: 7,
        newStart: 8,
        newEnd: 10,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 5,
        oldEnd: 10,
        newStart: 7,
        newEnd: 8,
        diffArr: [],
      },
      {
        operation: DiffOperation.DELETE,
        oldStart: 6,
        oldEnd: 6,
        newStart: 5,
        newEnd: 6,
        diffArr: [],
      },
    ];

    let result = instance.collapseAreas(areas);

    const expected: IAffectedArea[] = [
      {
        operation: DiffOperation.MIXED,
        oldStart: 5,
        oldEnd: 10,
        newStart: 5,
        newEnd: 10,
        diffArr: [],
      },
    ];

    expect(result).toEqual(expected);
  });

  it('should return 3 elements', () => {
    const areas: IAffectedArea[] = [
      {
        operation: DiffOperation.INSERT,
        oldStart: 0,
        oldEnd: 2,
        newStart: 0,
        newEnd: 3,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 1,
        oldEnd: 2,
        newStart: 2,
        newEnd: 2,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 5,
        oldEnd: 9,
        newStart: 6,
        newEnd: 10,
        diffArr: [],
      },
      {
        operation: DiffOperation.REPLACE,
        oldStart: 6,
        oldEnd: 10,
        newStart: 5,
        newEnd: 7,
        diffArr: [],
      },
      {
        operation: DiffOperation.INSERT,
        oldStart: 4,
        oldEnd: 4,
        newStart: 4,
        newEnd: 4,
        diffArr: [],
      },
      {
        operation: DiffOperation.DELETE,
        oldStart: 2,
        oldEnd: 3,
        newStart: 2,
        newEnd: 3,
        diffArr: [],
      },
    ];

    let result = instance.collapseAreas(areas);

    const expected: IAffectedArea[] = [
      {
        diffArr: [],
        newEnd: 3,
        newStart: 0,
        oldEnd: 3,
        oldStart: 0,
        operation: DiffOperation.MIXED,
      },
      {
        diffArr: [],
        newEnd: 4,
        newStart: 4,
        oldEnd: 4,
        oldStart: 4,
        operation: DiffOperation.INSERT,
      },
      {
        diffArr: [],
        newEnd: 10,
        newStart: 5,
        oldEnd: 10,
        oldStart: 5,
        operation: DiffOperation.MIXED,
      },

    ];

    expect(result).toEqual(expected);
  });
});
