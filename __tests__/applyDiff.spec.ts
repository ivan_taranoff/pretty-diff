import {applyDiff, DiffOperation, DiffResultElement} from "../src/jsStringDiff";


describe('function applyDiff', () => {


  it('single insert', () => {
    const str1 = '123';
    const str2 = '13';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.INSERT,
        start: 1,
        reference:1,
        length: 1,
        content: '2'
      }
    ]
    expect(applyDiff(str2, diff)).toBe(str1);
  })
  it('double insert', () => {
    const str1 = '12234456';
    const str2 = '123456';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.INSERT,
        start: 2,
        reference:2,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.INSERT,
        start: 5,
        reference:3,
        length: 1,
        content: '4'
      }
    ];
    expect(applyDiff(str2, diff)).toBe(str1);
  });
  it('single delete', () => {
    const str1 = '13';
    const str2 = '123';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        reference:1,
        length: 1,
        content: '2'
      }
    ];
    expect(applyDiff(str2, diff)).toBe(str1);
  });
  it('double delete', () => {
    const str1 = '13';
    const str2 = '1234';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        reference:1,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.DELETE,
        start: 2,
        reference:0,
        length: 1,
        content: '4'
      }
    ];
    expect(applyDiff(str2, diff)).toBe(str1);
  });
  it('single replace', () => {
    const str1 = '143';
    const str2 = '123';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        reference:1,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.INSERT,
        start: 1,
        reference:0,
        length: 1,
        content: '4'
      }
    ]
    expect(applyDiff(str2, diff)).toBe(str1);
  })
})
