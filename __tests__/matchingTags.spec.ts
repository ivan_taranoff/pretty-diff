import { PrettyDiff } from '../src';
import { TagRegexResult } from '../src/TagRegexResult';

describe('findOpeningTagFirstIndex() findClosingTagLastIndex()', () => {
  const comparator = new PrettyDiff();

  it('simple match forward tag', () => {
    const string = `<p class="head">q<i>we <b>bo</i>ld</b></p><p> zxc</p>`;
    // match <b>
    const endTagIndex = 38;
    const expectedIndex = 23;
    const tags = comparator.findTagsInString(string).map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
    const actual = comparator.findOpeningTagFirstIndex(
      tags,
      4,
    );

    expect(actual).toBe(expectedIndex);
  });

  it('simple match closing tag', () => {
    const string = `<p class="head">q<i>we <b>bo</i>ld</b></p><p> zxc</p>`;

    let startTagIndex = 17;
    let expectedIndex = 32;
    const tags = comparator.findTagsInString(string).map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
    let actual =
      comparator.findClosingTagLastIndex(tags, 1);

    expect(actual).toBe(expectedIndex);

    startTagIndex = 0;
    expectedIndex = 42;

    actual =
      comparator.findClosingTagLastIndex(tags, 0);

    expect(actual).toBe(expectedIndex);
  });

  it('complex match closing tag', () => {
    const string = `<p class="head"><p>q<i>we<p>1</p> <b>bo</i></p><p></p>ld</b></p><p> zxc</p>`;

    let startTagIndex = 0;
    let expectedIndex = 64;
    const tags = comparator.findTagsInString(string).map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));

    let actual =
      comparator.findClosingTagLastIndex(tags, 0);

    expect(actual).toBe(expectedIndex);
  });

  it('complex match opening tag', () => {
    const string = `<p class="head"><p>q<i>we<p>1</p> <b>bo</i></p><p></p>ld</b></p><p> zxc</p>`;

    let startTagIndex = 64;
    let expectedIndex = 0;
    const tags = comparator.findTagsInString(string).map((e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));

    let actual =
      comparator.findOpeningTagFirstIndex(tags, 11);

    expect(actual).toBe(expectedIndex);
  });
});
