import { performDiff } from '../src/jsStringDiff';

describe('function performDiff', () => {
  it('simple insert', () => {
    const str1 = '123';
    const str2 = '13';

    const result = {
      content: '2',
      length: 1,
      operation: 'insert',
      reference: 1,
      start: 1,
    };

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toContainEqual(result);
  });

  it('simple delete', () => {
    const str1 = '13';
    const str2 = '123';

    const result = {
      content: '2',
      length: 1,
      operation: 'delete',
      reference: 1,
      start: 1,
    };

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toContainEqual(result);
  });

  it('simple replace', () => {
    const str1 = '153';
    const str2 = '123';

    const result1 = {
      content: '2',
      length: 1,
      operation: 'delete',
      reference: 1,
      start: 1,
    };

    const result2 = {
      content: '5',
      length: 1,
      operation: 'insert',
      reference: 2,
      start: 1,
    };

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toEqual(expect.arrayContaining([result1, result2]));
  });

  it('empty params', () => {
    // const str1 = '13';
    // const str2 = '123';
    //
    // const result = {
    //   operation: 'delete',
    //   start: 1,
    //   length: 1,
    //   content: '2'
    // }
    //
    // const actualResult = performDiff(str1, str2);

    expect(performDiff('', '2')).toContainEqual({
      content: '2',
      length: 1,
      operation: 'delete',
      reference: 0,
      start: 0,
    });

    expect(performDiff('0', '')).toContainEqual({
      operation: 'insert',
      start: 0,
      reference: 0,
      length: 1,
      content: '0',
    });

    expect(performDiff('', '')).toEqual([]);
  });

  it('identical contents', () => {
    expect(performDiff('123456', '123456')).toEqual([]);
  });

  it('additional tests', () => {
    const expected = [
      {
        content: '334',
        length: 3,
        operation: 'delete',
        reference: 2,
        start: 2,
      },
      {
        content: '22',
        length: 2,
        operation: 'insert',
        reference: 5,
        start: 2,
      },
      {
        content: '78',
        length: 2,
        operation: 'delete',
        reference: 7,
        start: 6,
      },
      {
        content: 'a',
        length: 1,
        operation: 'insert',
        reference: 9,
        start: 6,
      },
      {
        content: '0987654321',
        length: 10,
        operation: 'insert',
        reference: 10,
        start: 8,
      },
    ];
    expect(performDiff('122256a90987654321', '1233456789')).toEqual(expected);
  });

  it('html test 1', () => {
    let oldString = `<p>abc qwe <b>bold</b> zxc</p>`;
    let newString = `<p class="head">q<i>we <b>bo</i>ld</b></p><p> zxc</p>`;
    let expected = [
      {
        content: '>ab',
        length: 3,
        operation: 'delete',
        reference: 2,
        start: 2,
      },
      {
        content: ' ',
        length: 1,
        operation: 'insert',
        reference: 5,
        start: 2,
      },
      {
        content: ' ',
        length: 1,
        operation: 'delete',
        reference: 6,
        start: 4,
      },
      {
        content: 'lass="head">',
        length: 12,
        operation: 'insert',
        reference: 7,
        start: 4,
      },
      {
        content: '<i>',
        length: 3,
        operation: 'insert',
        reference: 8,
        start: 17,
      },
      {
        content: '</i>',
        length: 4,
        operation: 'insert',
        reference: 16,
        start: 28,
      },
      {
        content: '></p><p',
        length: 7,
        operation: 'insert',
        reference: 21,
        start: 37,
      },
    ];
    expect(performDiff(newString, oldString)).toEqual(expected);
  });
});
