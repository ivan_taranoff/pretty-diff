import { performDiffCompact } from '../src/jsStringDiff';

describe.skip('performDiffCompact()', () => {
  it('compacts', () => {
    let oldString = `<p>abc qwe <b>bold</b> zxc</p>`;
    let newString = `<p class="head">q<i>we <b>bo</i>ld</b></p><p> zxc</p>`;
    const expected = [
      {
        content: ' ',
        length: 1,
        contentDeleted: '>ab',
        lengthDeleted: 3,
        operation: 'replace',
        start: 2,
      },
      {
        content: 'lass="head">',
        length: 12,
        contentDeleted: ' ',
        lengthDeleted: 1,
        operation: 'replace',
        start: 4,
      },
      {
        content: '<i>',
        length: 3,
        operation: 'insert',
        start: 17,
      },
      {
        content: '</i>',
        length: 4,
        operation: 'insert',
        start: 28,
      },
      {
        content: '></p><p',
        length: 7,
        operation: 'insert',
        start: 37,
      },
    ];
    expect(performDiffCompact(newString, oldString)).toEqual(expected);
  });

  it('compacts - edge case 1', () => {
    const prev = `<p>asd<span class="asd">value</span><span>another</span></p>`;
    const next = `<p>asd<b class="asd">value</b><span>another</span></p>`;
    const actual = performDiffCompact(next, prev);

    const expected = [
      {
        content: 'b',
        contentDeleted: 'span',
        length: 1,
        lengthDeleted: 4,
        operation: 'replace',
        start: 7,
      },
      {
        content: 'b',
        contentDeleted: 'span',
        length: 1,
        lengthDeleted: 4,
        operation: 'replace',
        start: 28,
      },
    ];
    expect(actual).toEqual(expected);
  });
});
