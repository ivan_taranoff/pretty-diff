import { ITagRegexResult } from './ITagRegexResult';

export class TagRegexResult implements ITagRegexResult {
  public close: boolean;
  public index: number;
  public open: boolean;
  public self: boolean;
  public tag: string;
  public tagName: string;
  public tagLength: number;
  public replace: boolean;

  public constructor(
    tag: string,
    index: number,
    other?: Partial<ITagRegexResult>,
  ) {
    this.tag = tag; // full tag match
    this.index = index;
    this.replace = false;
    this.tagName = '';
    this.open = false;
    this.close = false;
    this.self = false;
    if (other) {
      this.tagName = other.tagName.toLowerCase() || '';
      this.open = other.open || false;
      this.close = other.close || false;
      this.self = other.self || false;
      this.replace = other.replace || false;
    }
  }

  public static FROM_REGEX_EXEC_ARRAY(
    input: RegExpExecArray,
    shiftIndexBy: number = 0,
  ): ITagRegexResult {
    let result = {
      tagName: '',
      tag: input[0],
      open: false,
      close: false,
      self: false,
      index: input.index + shiftIndexBy,
      tagLength: input[0].length,
      replace: false,
    };

    if (input.groups) {
      if (input.groups['open'] || input.groups['open2']) {
        result.tagName = input.groups['open'] || input.groups['open2'];
        result.open = true;
      } else if (input.groups['self']) {
        result.tagName = input.groups['self'];
        result.self = true;
      } else if (input.groups['close']) {
        result.tagName = input.groups['close'];
        result.close = true;
      }
    }
    return result;
  }
}
