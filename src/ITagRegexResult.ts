export interface ITagRegexResult {
  tag: string;
  tagLength: number;
  tagName: string;
  index: number;
  open: boolean;
  close: boolean;
  self: boolean;
  replace: boolean;
}

