import { DiffResultElement } from './jsStringDiff';

export function findBackwardIndexOf(
  heap: string,
  needle: string,
  start: number,
): number {
  return heap.slice(0, start).lastIndexOf(needle);
}

export function findForwardIndexOf(
  heap: string,
  needle: string,
  start: number,
): number {
  const result = heap.slice(start + 1).indexOf(needle);
  return ~result ? result + start + 1 : -1;
}

export function findLastIndex<T>(heap:T[], predicate:(e:T)=>boolean):number {
  const index = heap.slice().reverse().findIndex(predicate);
  const count = heap.length - 1;
  return index >= 0 ? count - index : index;
}

export function removeWhitespace(input: string) {
  return input
    .replace(/([ \t][ \t]+)/g, ' ')
    .replace(/^[ \t]*/, '');
}

export function adjustDiffResults(diffResults: DiffResultElement[],oldContent:string): DiffResultElement[] {
  return diffResults.map((e: DiffResultElement) => {
    if (e.content && e.content.length > 0) {
      if (
        e.content[e.content.length - 1] === '<' &&
        // e.content[e.start - 1] === '<'
        oldContent[e.reference - 1] === '<'
      ) {
        return <DiffResultElement>{
          ...e,
          content: '<' + e.content.slice(0, -1),
          start: e.start - 1,
          reference: e.reference - 1,
        };
      } else if (
        e.content[0] === '>' &&
        // e.content[e.start + e.content.length] === '>'
        oldContent[e.reference + e.content.length] === '>'
      ) {
        return <DiffResultElement>{
          ...e,
          content: e.content.slice(1) + '>',
          start: e.start + 1,
          reference: e.reference + 1,
        };
      } else return e;
    } else return e;
  });
}

export function everyMapValuesIsZero(input: Map<string, number>) {
  for (let v of input.values()) {
    if (v !== 0) {
      return false;
    }
  }
  return true;
}

export function everyObjectValueIsZero(input: { [key: string]: number }) {
  let keys = Object.keys(input);
  for (let k of keys) {
    if (input[k] !== 0) {
      return false;
    }
  }
  return true;
}

export function someObjectValuePositive(input: { [key: string]: number }) {
  let keys = Object.keys(input);
  for (let k of keys) {
    if (input[k] > 0) {
      return true;
    }
  }
  return false;
}

export function someObjectValueNegative(input: { [key: string]: number }) {
  let keys = Object.keys(input);
  for (let k of keys) {
    if (input[k] < 0) {
      return true;
    }
  }
  return false;
}
