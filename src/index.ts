import { DiffOperation, DiffResultElement, performDiff } from './jsStringDiff';
import {
  adjustDiffResults,
  everyObjectValueIsZero,
  findBackwardIndexOf,
  findForwardIndexOf,
  findLastIndex,
  removeWhitespace,
  someObjectValuePositive,
} from './util';
import { TagRegexResult } from './TagRegexResult';
import { ITagRegexResult } from './ITagRegexResult';
import { prependListener } from 'cluster';

export interface IAffectedArea {
  oldStart: number;
  newStart: number;
  oldEnd: number;
  newEnd: number;
  diffArr: DiffResultElement[];
  operation: DiffOperation;
}

type TagLocation = {
  start: number;
  tagString: string;
  replace?: boolean;
  replaceLength?: number;
};

enum TagType {
  INSERT_START = '<ins>',
  INSERT_END = '</ins>',
  DELETE_START = '<del>',
  DELETE_END = '</del>',
  REPLACE_START = '<repl>',
  REPLACE_END = '</repl>',
}

export interface IRange {
  start: number;
  end: number;
}

export interface IRangeTag extends IRange {
  // start: number;
  // end: number;
  startTag?: ITagRegexResult;
  endTag?: ITagRegexResult;
}

export interface IPrettyDiffResult {
  prevContent: string;
  newContent: string;
}

export interface IPrettyDiffOptions {
  ignoreWhitespace: boolean;
  onlyBody: boolean;
}

export class PrettyDiff {
  public fileNewContents: string;

  public filePreviousContents: string;

  private previousTags: ITagRegexResult[];
  private newTags: ITagRegexResult[];

  private defaultOptions: IPrettyDiffOptions = {
    ignoreWhitespace: false,
    onlyBody: true,
  };
  private readonly options: IPrettyDiffOptions;

  constructor(
    options?: Partial<IPrettyDiffOptions>,
    newContent: string = '',
    oldContent: string = '',
  ) {
    this.options = { ...this.defaultOptions, ...options };
    this.fileNewContents = newContent;
    this.filePreviousContents = oldContent;
  }

  public process(
    prev: string = this.filePreviousContents,
    next: string = this.fileNewContents,
  ): {
    prevContent: string;
    newContent: string;
  } {
    let prevDoctype,
      newDoctype,
      originalPrev,
      originalNext,
      xhtmlPrev,
      xhtmlNext;
    originalPrev = prev;
    originalNext = next;
    let window = undefined;
    if (window !== undefined) {
      const parser = new DOMParser();
      const serializer = new XMLSerializer();
      try {
        const docPrev = parser.parseFromString(originalPrev, 'text/html');
        const docNext = parser.parseFromString(originalNext, 'text/html');
        xhtmlPrev = serializer.serializeToString(docPrev);
        xhtmlNext = serializer.serializeToString(docNext);
      } catch (e) {
        console.error(e);
        xhtmlPrev = prev;
        xhtmlNext = next;
      }
    } else {
      xhtmlPrev = originalPrev;
      xhtmlNext = originalNext;
    }
    prevDoctype = this.cutDoctype(xhtmlPrev);
    newDoctype = this.cutDoctype(xhtmlNext);
    this.filePreviousContents = prevDoctype.content;
    this.fileNewContents = newDoctype.content;

    let prevBeforeBody = '',
      prevAfterBody = '',
      newBeforeBody = '',
      newAfterBody = '';
    if (this.options.onlyBody) {
      const regex = /^(.*<\s*body[^>]*>)(.*)(<\s*\/\s*body\s*>.*)$/s;
      let matches = regex.exec(this.filePreviousContents);
      if (matches) {
        [, prevBeforeBody, this.filePreviousContents, prevAfterBody] = matches;
      }

      matches = regex.exec(this.fileNewContents);
      if (matches) {
        [, newBeforeBody, this.fileNewContents, newAfterBody] = matches;
      }
    }

    if (this.options.ignoreWhitespace) {
      this.filePreviousContents = removeWhitespace(this.filePreviousContents);
      this.fileNewContents = removeWhitespace(this.filePreviousContents);
    }

    const diff: DiffResultElement[] = performDiff(
      this.fileNewContents,
      this.filePreviousContents,
    );

    const diffFixed = adjustDiffResults(diff, this.filePreviousContents);

    this.previousTags = this.findTagsInString(this.filePreviousContents).map(
      (e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e),
    );
    this.newTags = this.findTagsInString(this.fileNewContents).map(
      (e: RegExpExecArray) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e),
    );

    let diffAreas = this.calculateAffectedAreas(diffFixed);

    // пока накостылил - надо дважды вызывать коллапс, минимальной оптимищации ради
    // not anymore
    let diffAreasCollapsed = this.collapseAreas(diffAreas);

    let fixedTables = this.handleTables(diffAreasCollapsed);

    // now - insert modification tags. and that`s all!
    let result = this.insertModTags(fixedTables);

    // add pre and post body tag content
    if (this.options.onlyBody) {
      result.prevContent = prevBeforeBody + result.prevContent + prevAfterBody;
      result.newContent = newBeforeBody + result.newContent + newAfterBody;
    }

    // add deleted doctype tag
    result.prevContent = prevDoctype.doctype + result.prevContent;
    result.newContent = newDoctype.doctype + result.newContent;
    return result;
  }

  private cutDoctype(input: string): { content: string; doctype: string } {
    const regex = /^(.*<\s*!\s*DOCTYPE[^>]*>)/s;
    const result = regex.exec(input);

    return {
      content: input.replace(regex, ''),
      doctype: (result && result[0]) || '',
    };
  }

  /**
   * Вставляет тэга в обе версии контента
   * @param diffAreas
   * @param prev
   * @param next
   */
  public insertModTags(
    diffAreas: IAffectedArea[],
    prev: string = this.filePreviousContents,
    next: string = this.fileNewContents,
  ): IPrettyDiffResult {
    let prevContent = prev;
    let newContent = next;
    // DESC sort
    let sortedByOldEnd: IAffectedArea[] = diffAreas.sort(
      (a: IAffectedArea, b: IAffectedArea) => b.oldEnd - a.oldEnd,
    );
    // DESC sort
    let sortedByNewEnd: IAffectedArea[] = diffAreas.sort(
      (a: IAffectedArea, b: IAffectedArea) => b.newEnd - a.newEnd,
    );

    const perform = (
      array: IAffectedArea[],
      content: string,
      prop: string,
    ): string => {
      return array
        .flatMap((e: IAffectedArea): TagLocation[] => {
          let tagStart: TagType, tagEnd: TagType;

          switch (e.operation) {
            case DiffOperation.DELETE:
              tagStart = TagType.DELETE_START;
              tagEnd = TagType.DELETE_END;
              break;
            case DiffOperation.INSERT:
              tagStart =
                prop === 'old' ? TagType.DELETE_START : TagType.INSERT_START;
              tagEnd = prop === 'old' ? TagType.DELETE_END : TagType.INSERT_END;
              break;
            case DiffOperation.MIXED:
            case DiffOperation.REPLACE:
              tagStart = TagType.REPLACE_START;
              tagEnd = TagType.REPLACE_END;
              break;
          }
          return [
            { start: e[prop + 'End'], tagString: tagEnd },
            { start: e[prop + 'Start'], tagString: tagStart },
          ];
        })
        .concat(
          (prop === 'new' ? this.newTags : this.previousTags)
            .filter((e: ITagRegexResult) => e.replace)
            .map(
              (e: ITagRegexResult): TagLocation => {
                return {
                  start: e.index,
                  tagString: e.tag.replace(/\s*(\/?\s*>)$/, ` replace$1`),
                  replace: true,
                  replaceLength: e.tag.length,
                };
              },
            ),
        )
        .sort((a: TagLocation, b: TagLocation) => b.start - a.start)
        .reduce((acc: string, el: TagLocation): string => {
          return (
            acc.substring(0, el.start) +
            el.tagString +
            acc.substring(
              el.start +
              (el.replace && el.replaceLength ? el.replaceLength : 0),
            )
          );
        }, content);
    };

    prevContent = perform(sortedByOldEnd, prevContent, 'old');
    newContent = perform(sortedByNewEnd, newContent, 'new');

    return { prevContent, newContent };
  }

  public calculateAffectedAreas(diff: DiffResultElement[]): IAffectedArea[] {
    return diff.map<IAffectedArea>((e: DiffResultElement) => {
      return this.findAffectedArea(e);
    });
  }

  /**
   * Проверяет пересекается ли элемент A c элементом B,
   * @param a{IAffectedArea}
   * @param b{IAffectedArea}
   * @return {boolean}
   */
  private static areaIntersects(a: IAffectedArea, b: IAffectedArea): boolean {
    let ANinBN = a.newEnd <= b.newEnd && a.newStart >= b.newStart;
    let BNinAN = b.newEnd <= a.newEnd && b.newStart >= a.newStart;
    let AOinBO = a.oldEnd <= b.oldEnd && a.oldStart >= b.oldStart;
    let BOinAO = b.oldEnd <= a.oldEnd && b.oldStart >= a.oldStart;
    let ANEinBN = a.newEnd <= b.newEnd && a.newEnd >= b.newStart;
    let ANSinBN = a.newStart <= b.newEnd && a.newStart >= b.newStart;
    let AOEinBO = a.oldEnd <= b.oldEnd && a.oldEnd >= b.oldStart;
    let AOSinBO = a.oldStart <= b.oldEnd && a.oldStart >= b.oldStart;

    return (
      ANinBN ||
      BNinAN ||
      AOinBO ||
      BOinAO ||
      ANEinBN ||
      ANSinBN ||
      AOEinBO ||
      AOSinBO
    );
  }

  /**
   * Проверяет пересекается ли элемент A c элементом B,
   * @param a{IRange}
   * @param b{IRange}
   * @return {boolean}
   */
  private static rangeIntersects(a: IRange, b: IRange) {
    let ANinBN = a.start <= b.end && a.start >= b.start;
    let BNinAN = b.start <= a.end && b.start >= a.start;
    return ANinBN || BNinAN;
  }

  /**
   * Схлопывает/сворачивает пересакающиеся регионы
   * таким образом мы должны получить регионы в хтмл коде
   * которые могут безболезненно заменять друг друга
   * @param areas
   */
  public collapseAreas(areas: IAffectedArea[]): IAffectedArea[] {
    let result = [...areas];
    for (let i = 1; i < result.length; i++) {
      for (let c = 0; c < i && c < result.length; c++) {
        if (PrettyDiff.areaIntersects(result[c], result[i])) {
          result[c] = getIntersectedElement(result[c], result[i]);
          result.splice(i, 1);
          i--; // keep iterator in right place
        }
      }
    }

    return result.sort(
      (a: IAffectedArea, b: IAffectedArea) => a.newStart - b.newStart,
    );

    /**
     * Возвращает пересечение областей
     * @param a
     * @param b
     */
    function getIntersectedElement(
      a: IAffectedArea,
      b: IAffectedArea,
    ): IAffectedArea {
      let acc = { ...a };
      acc.newEnd = Math.max(acc.newEnd, b.newEnd);
      acc.newStart = Math.min(acc.newStart, b.newStart);
      acc.oldEnd = Math.max(acc.oldEnd, b.oldEnd);
      acc.oldStart = Math.min(acc.oldStart, b.oldStart);
      if (acc.operation !== b.operation) {
        acc.operation = DiffOperation.MIXED;
      }
      acc.diffArr = [...acc.diffArr, ...b.diffArr];
      return acc;
    }
  }

  /**
   * Вообще, этод метод должен был возвращать "референсы". Т.е. те позиции в
   * старой строке в которых были изменения. Вычисление этих мест выло вынесено
   * в performDiff
   * @deprecated
   * @param diff
   */
  public calculatePreviousFilePositions(diff: DiffResultElement[]): number[] {
    // let result: number[] = [];

    return diff.map(
      (e: DiffResultElement, i: number, a: DiffResultElement[]) => {
        // let prevFileStartPos: number = e.start;
        // if (i > 0) {
        //   prevFileStartPos = a
        //     .slice(0, i - 1)
        //     .reduce<number>((acc: number, diffItem: DiffResultElement) => {
        //       if (diffItem.operation === DiffOperation.DELETE)
        //         acc += diffItem.length;
        //       else if (diffItem.operation === DiffOperation.INSERT)
        //         acc -= diffItem.length;
        //       else if (diffItem.operation === DiffOperation.REPLACE)
        //         acc += diffItem.lengthDeleted - diffItem.length;
        //       return acc;
        //     }, e.start);
        // }
        // result.push(prevFileStartPos);
        // if (i > 0) {
        return a
          .slice(0, i)
          .reduce((acc: number, diffItem: DiffResultElement) => {
            if (diffItem.operation === DiffOperation.DELETE)
              acc -= diffItem.length;
            // acc = acc;
            else if (diffItem.operation === DiffOperation.INSERT)
              acc += diffItem.length;
            else if (diffItem.operation === DiffOperation.REPLACE)
              acc += diffItem.length - diffItem.lengthDeleted;
            return acc;
          }, e.start);

        // } else
        //   return e.start;
      },
    );
  }

  // tslint:disable-next-line:max-func-body-length
  public handleTables(areasInput: IAffectedArea[]): IAffectedArea[] {
    //find table areas
    const tableTagsPrev = this.previousTags.filter(
      (tag: ITagRegexResult) => tag.tagName === 'table',
    );
    const tableTagsNew = this.newTags.filter(
      (tag: ITagRegexResult) => tag.tagName === 'table',
    );

    const areas = [...areasInput];

    function getRanges(arr: ITagRegexResult[]) {
      return arr.reduce(
        (a: IRangeTag[], e: ITagRegexResult, i: number): IRangeTag[] => {
          if (e.open) {
            const result = PrettyDiff.findMatchingTag(arr.slice(i));
            if (result !== null) {
              a.push({
                start: e.index,
                end: result.index + result.tagLength,
                startTag: e,
                endTag: result,
              });
            }
          }
          return a;
        },
        <IRangeTag[]>[],
      );
    }

    const tableRangesPrev: IRangeTag[] = getRanges(tableTagsPrev);

    const tableRangesNew: IRangeTag[] = getRanges(tableTagsNew);

    const extendedAreas = areas.map((a: IAffectedArea) => {
      let result = { ...a };
      const foundTableRangePrev = tableRangesPrev.find((r: IRange) =>
        PrettyDiff.rangeIntersects(
          {
            start: a.oldStart,
            end: a.oldEnd,
          },
          r,
        ),
      );

      if (foundTableRangePrev) {
        result.oldStart = result.oldStart > foundTableRangePrev.startTag.index ? foundTableRangePrev.startTag.index : result.oldStart;
        result.oldEnd = result.oldEnd < (foundTableRangePrev.endTag.index + foundTableRangePrev.endTag.tagLength) ? foundTableRangePrev.endTag.index + foundTableRangePrev.endTag.tagLength : result.oldEnd;
      }

      const foundTableRangeNew = tableRangesNew.find((r: IRange) =>
        PrettyDiff.rangeIntersects(
          {
            start: a.newStart,
            end: a.newEnd,
          },
          r,
        ),
      );

      if (foundTableRangeNew) {
        result.newStart = result.newStart > foundTableRangeNew.startTag.index ? foundTableRangeNew.startTag.index : result.newStart;
        result.newEnd = result.newEnd < (foundTableRangeNew.endTag.index + foundTableRangeNew.endTag.tagLength) ? foundTableRangeNew.endTag.index + foundTableRangeNew.endTag.tagLength : result.newEnd;
      }

      return result;
    });

    return this.collapseAreas(extendedAreas);


    // return areas.filter((a: IAffectedArea) => {
    //   const foundTableRangePrev = tableRangesPrev.find((r: IRange) =>
    //     PrettyDiff.rangeIntersects(
    //       {
    //         start: a.oldStart,
    //         end: a.oldEnd,
    //       },
    //       r,
    //     ),
    //   );
    //   if (
    //     !foundTableRangePrev ||
    //     foundTableRangePrev.start >= a.oldStart ||
    //     foundTableRangePrev.end <= a.oldEnd
    //   ) {
    //     // table tag is inside of diff
    //     return true; // do nothing whole table would be deleted or inserted
    //   }
    //
    //   const foundTableRangeNew = tableRangesNew.find((r: IRange) =>
    //     PrettyDiff.rangeIntersects(
    //       {
    //         start: a.newStart,
    //         end: a.newEnd,
    //       },
    //       r,
    //     ),
    //   );
    //   if (
    //     !foundTableRangeNew ||
    //     foundTableRangeNew.start >= a.oldStart ||
    //     foundTableRangeNew.end <= a.oldEnd
    //   ) {
    //     // table tag is inside of diff
    //     return true; // do nothing whole table would be deleted or inserted
    //   }
    //   // now find TR tags and verify that 'a' falls in between those tags
    //   const foundTableRangePrevTRs = getRanges(
    //     this.previousTags.filter(
    //       (e: ITagRegexResult) =>
    //         e.index >= foundTableRangePrev.start &&
    //         e.index + e.tagLength < foundTableRangePrev.end &&
    //         e.tagName === 'tr',
    //     ),
    //   );
    //   const foundTableRangeNextTRs = getRanges(
    //     this.newTags.filter(
    //       (e: ITagRegexResult) =>
    //         e.index >= foundTableRangePrev.start &&
    //         e.index + e.tagLength < foundTableRangePrev.end &&
    //         e.tagName === 'tr',
    //     ),
    //   );
    //
    //   const affectedPrevTR = foundTableRangePrevTRs.find(
    //     (e: IRangeTag) => e.start < a.oldStart && e.end > a.oldEnd,
    //   );
    //   const affectedNextTR = foundTableRangeNextTRs.find(
    //     (e: IRangeTag) => e.start < a.oldStart && e.end > a.oldEnd,
    //   );
    //   if (affectedNextTR && affectedPrevTR) {
    //     affectedNextTR.startTag.replace = true;
    //     affectedPrevTR.startTag.replace = true;
    //     return false;
    //   }
    //   if (!affectedPrevTR && !affectedNextTR) {
    //     foundTableRangePrev.startTag.replace = true;
    //     foundTableRangeNew.startTag.replace = true;
    //     return false;
    //   }
    //
    //   return true;
    //   // if not - mark whole table to be replaced;
    //
    //   // console.log({
    //   //   foundTableRangeNew,
    //   //   foundTableRangePrev,
    //   //   a,
    //   //   foundTableRangeNextTRs,
    //   //   foundTableRangePrevTRs,
    //   // });
    // });

    //replace areas

    // return [];
  }

  // tslint:disable-next-line:max-func-body-length
  public findAffectedArea(diff: DiffResultElement): IAffectedArea | null {
    // at first check if we are in the middle of the tag
    let insideOfTagIndexPrev = this.previousTags.findIndex(
      (e: ITagRegexResult) =>
        e.index < diff.reference && e.index + e.tagLength > diff.reference,
    );
    let insideOfTagIndexNew = this.newTags.findIndex(
      (e: ITagRegexResult) =>
        e.index < diff.start && e.index + e.tagLength > diff.start,
    );

    if (~insideOfTagIndexPrev || ~insideOfTagIndexNew) {
      let result = {
        oldStart: diff.reference,
        oldEnd: diff.reference,
        newStart: diff.start,
        newEnd: diff.start,
        operation: diff.operation,
        diffArr: [diff],
      };
      if (diff.operation === DiffOperation.INSERT) {
        result.newEnd += diff.length;
      } else if (diff.operation === DiffOperation.DELETE) {
        result.oldEnd += diff.length;
      }
      if (~insideOfTagIndexNew) {
        let newContentAffectedRange = this.findCompleteTagRange(this.newTags, {
          start: result.newStart,
          end: result.newEnd,
        });
        // const newContentAffectedRange = this.processInsideOfTagCase(
        //   this.newTags,
        //   insideOfTagIndexNew,
        // );
        // result.newEnd = newContentAffectedRange.end;
        // result.newStart = newContentAffectedRange.start;
        result.newStart = Math.min(
          newContentAffectedRange.start,
          result.newStart,
        );
        result.newEnd = Math.max(newContentAffectedRange.end, result.newEnd);
      }
      if (~insideOfTagIndexPrev) {
        // const prevContentAffectedRange = this.processInsideOfTagCase(
        //   this.previousTags,
        //   insideOfTagIndexPrev,
        // );
        // result.oldEnd = prevContentAffectedRange.end;
        // result.oldStart = prevContentAffectedRange.start;
        let prevContentAffectedRange = this.findCompleteTagRange(
          this.previousTags,
          {
            start: result.oldStart,
            end: result.oldEnd,
          },
        );
        result.oldStart = Math.min(
          prevContentAffectedRange.start,
          result.oldStart,
        );
        result.oldEnd = Math.max(prevContentAffectedRange.end, result.oldEnd);
      }
      return result;
    } else if (this.diffContainsTags(diff)) {
      // also not a good case
      // check if those tags close each other

      // let result = {
      //   oldStart: diff.start,
      //   oldEnd: diff.start,
      //   newStart: diff.reference,
      //   newEnd: diff.reference,
      //   operation: diff.operation,
      //   diffArr: [diff],
      // };
      let result = {
        oldStart: diff.reference,
        oldEnd: diff.reference,
        newStart: diff.start,
        newEnd: diff.start,
        operation: diff.operation,
        diffArr: [diff],
      };

      if (
        diff.operation === DiffOperation.INSERT // ||
      // diff.operation === DiffOperation.REPLACE
      ) {
        result.newEnd += diff.length;
        let range = this.findCompleteTagRange(this.newTags, {
          start: result.newStart,
          end: result.newEnd,
        });
        result.newStart = Math.min(range.start, diff.start);
        result.newEnd = Math.max(range.end, diff.start + diff.length);
      }

      if (
        diff.operation === DiffOperation.DELETE // ||
      // diff.operation === DiffOperation.REPLACE
      ) {
        // const tagsInsidePrevContent = this.previousTags.filter((e: ITagRegexResult) => e.index >= diff.start && ((e.index + e.tagLength) < diff.start + diff.length));
        result.oldEnd += diff.length;
        let range = this.findCompleteTagRange(this.previousTags, {
          start: result.oldStart,
          end: result.oldEnd,
        });
        if (range.leftShift > 0) {
          // maybe implement that tech to a case of insideOfTag

          // we need to expand new file area by a number of tags old file was expended to the left
          // let currentShift = 1;
          let tagIndexFollowingStart = this.newTags.findIndex(
            (e: ITagRegexResult) => e.index + e.tagLength > result.newStart,
          );
          if (tagIndexFollowingStart - range.leftShift < 0) {
            console.error(
              'Found leftShift is too big, results in going below 0 index,',
              tagIndexFollowingStart - range.leftShift,
            );
            return result;
          }
          // it should form a closed (complete) tag
          const newRange: IRange = {
            start: this.newTags[tagIndexFollowingStart - range.leftShift].index,
            end: result.newEnd,
          };

          let newRangeExtended = this.findCompleteTagRange(
            this.newTags,
            newRange,
          );
          result.newStart = Math.min(newRangeExtended.start, result.newStart);
          result.newEnd = Math.max(newRangeExtended.end, result.newEnd);
          // at it would result in a REPLACE operation
          result.operation = DiffOperation.REPLACE;
        }
        result.oldStart = Math.min(range.start, diff.reference);
        result.oldEnd = Math.max(range.end, diff.reference + diff.length);
        // result.oldStart = range.start;
        // result.oldEnd = range.end;
      }

      return result;
    } else {
      // perfect case we are out of tags. just a plain text
      if (diff.operation === DiffOperation.INSERT) {
        return {
          oldStart: diff.reference,
          oldEnd: diff.reference,
          newStart: diff.start,
          newEnd: diff.start + diff.length,
          operation: diff.operation,
          diffArr: [diff],
        };
      } else if (diff.operation === DiffOperation.DELETE) {
        return {
          oldStart: diff.reference,
          oldEnd: diff.reference + diff.length,
          newStart: diff.start,
          newEnd: diff.start,
          operation: diff.operation,
          diffArr: [diff],
        };
      }
      // } else if (diff.operation === DiffOperation.REPLACE) {
      //   return {
      //     oldStart: diff.start,
      //     oldEnd: diff.start + diff.lengthDeleted,
      //     newStart: diff.reference,
      //     newEnd: diff.reference + diff.length,
      //     operation: diff.operation,
      //     diffArr: [diff],
      //   };
      // }
    }

    return null;
  }

  /**
   * @deprecated
   * @param tags
   * @param tagIndex
   */
  public processInsideOfTagCase(
    tags: ITagRegexResult[],
    // diff: DiffResultElement,
    tagIndex: number,
  ): IRange {
    const tag = tags[tagIndex];

    let result: IRange = {
      start: tag.index,
      end: tag.index + tag.tagLength,
    };

    // find Tag Start
    // const tagStart = findBackwardIndexOf(fileContents, '<', startIndex);
    // //find tag name
    // const tagNameRegexResult = /^<\s*\/*\s*([a-z0-9]+)[^>]*>/i.exec(
    //   fileContents.slice(tagStart),
    // );
    // check if they are selfclosing
    // let selfClosing = this.isTagSelfclosing(tagString);
    if (tag.self) {
      // we are lucky - just set result params
      result.start = tag.index;
      result.end = tag.index + tag.tagLength;
    } else if (tag.open) {
      // find closing tag taking into respect that there may be opening tags too
      let closingTagIndex = this.findClosingTagLastIndex(tags, tagIndex);
      if (~closingTagIndex) {
        result.start = tag.index;
        result.end = closingTagIndex;
      } else {
        console.error('ERROR 106 - no matching tag found for ', tag);
      }
    } else if (tag.close) {
      let indexShift = this.findOpeningTagFirstIndex(tags, tagIndex);
      if (~indexShift) {
        result.start = indexShift;
        result.end = tag.index + tag.tagLength;
      } else {
        console.error('ERROR 108 - no matching tag found for ', tag);
      }
    }
    return result;
  }

  public isInsideOfTag(contents: string, startPos: number): boolean {
    // changed to include closing tag brackets
    // consider to change it to also match opening brackets
    // if (contents[startPos] === '<' || contents[startPos] === '>') return true;
    if (contents[startPos] === '>') return true;
    const fwdL = findForwardIndexOf(contents, '>', startPos);
    const fwdR = findForwardIndexOf(contents, '<', startPos);
    const bwdL = findBackwardIndexOf(contents, '>', startPos);
    const bwdR = findBackwardIndexOf(contents, '<', startPos);
    return !!(~fwdL && ~bwdR && (fwdL < fwdR || !~fwdR) && bwdR > bwdL);
  }

  public diffContainsTags(diff: DiffResultElement): boolean {
    // return /(<[^>]+>)/gi.test(diff.content);
    return /[><]/gi.test(diff.content);
  }

  // private in fact, solely for testing
  /**
   * Ищет вперед закрывающий тег
   * @param {ITagRegexResult[]} tags - массив тего в документе
   * @param {number} startIndex - начальный индекс в tags для поиска закрывающего тэга
   * @return {number} Индекс последнего символа соответствующего закрывающего тэга
   */
  public findClosingTagLastIndex(
    tags: ITagRegexResult[],
    startIndex: number,
  ): number {
    // just count tags
    const tag = tags[startIndex];
    let result = PrettyDiff.findMatchingTag(
      tags
        .slice(startIndex)
        .filter((e: ITagRegexResult) => e.tagName === tag.tagName),
    );
    if (result !== null) {
      return result.index + result.tagLength;
    }
    return -1;
  }

  /**
   * Ищет назад открывающий тег
   * @param {ITagRegexResult[]} tags - массив тего в документе
   * @param {number} startIndex - конечный индекс в tags для поиска открывающего тэга
   * @return {number} Индекс первого символа соответствующего открывающего тэга
   */
  public findOpeningTagFirstIndex(
    tags: ITagRegexResult[],
    startIndex: number,
  ): number {
    // reverse search
    const tag = tags[startIndex];

    let result = PrettyDiff.findMatchingTag(
      tags
        .slice(0, startIndex + 1)
        .filter((e: ITagRegexResult) => e.tagName === tag.tagName)
        .reverse(),
    );
    if (result !== null) {
      return result.index;
    }

    return -1;
  }

  /**
   * Принимает массив тэгов. Тэги должны быть одного имени.
   * @param tags
   * @returns {RegExpExecArray|null} - найденный тэг или null если не найден такой тэг
   */
  private static findMatchingTag(
    tags: ITagRegexResult[],
  ): ITagRegexResult | null {
    let currentTagCount = 0; // because we start

    for (let i = 0; i < tags.length; i++) {
      if (tags[i].open) {
        currentTagCount++;
      } else if (tags[i].close) {
        currentTagCount--;
      }
      if (currentTagCount === 0) {
        return tags[i];
      }
    }
    return null;
  }

  public findOpeningTagIndex(
    tags: ITagRegexResult[],
    startIndex: number,
  ): number {
    const search = tags[startIndex];
    if (!search.close) {
      return -1;
    }
    let currentTagCount = 0;
    for (let i = startIndex; i >= 0; i--) {
      if (tags[i].tagName !== search.tagName) continue;
      if (tags[i].open) {
        currentTagCount++;
      } else if (tags[i].close) {
        currentTagCount--;
      }
      if (currentTagCount === 0) {
        return i;
      }
    }

    return -1;
  }

  // tslint:disable-next-line:max-func-body-length
  public findCompleteTagRange(
    tags: ITagRegexResult[],
    initialRange: IRange,
  ): IRange & { leftShift: number } {
    let resultRange: IRange & { leftShift: number } = {
      ...initialRange,
      leftShift: 0,
    };

    let currentIndex = tags.findIndex(
      (e: ITagRegexResult) => e.index + e.tagLength > resultRange.start,
    );

    // if start is inside of a tag - adjust start to the beginning of the tag
    if (resultRange.start > tags[currentIndex].index)
      resultRange.start = tags[currentIndex].index;
    if (
      resultRange.end <
      tags[currentIndex].index + tags[currentIndex].tagLength
    ) {
      resultRange.end = tags[currentIndex].index + tags[currentIndex].tagLength;
    }

    // check if initial range contains only selfClosing tags

    let selfClosingCheck = tags
      .filter(
        (e: ITagRegexResult) =>
          e.index + e.tagLength > resultRange.start &&
          e.index < resultRange.end,
      )
      .map((e: ITagRegexResult) => {
        // adjust resultRange in case we have only selfclosing tags and range ends in the middle of a tag
        if (resultRange.start > e.index) resultRange.start = e.index;
        if (resultRange.end < e.index + e.tagLength) {
          resultRange.end = e.index + e.tagLength;
        }
        return e;
      })
      .reduce((a: boolean, e: ITagRegexResult) => a && e.self, true);
    if (selfClosingCheck) return resultRange;

    let map: { [key: string]: number } = Object.create(null);
    let leftIndex = tags.findIndex(
      (e: ITagRegexResult) => e.index + e.tagLength > resultRange.start,
    );
    let rightIndex = findLastIndex(
      tags,
      (e: ITagRegexResult) => e.index < resultRange.end,
    );

    if (!~leftIndex || !~rightIndex) {
      console.error('Unable to find suitable tag', { resultRange, tags });
      return resultRange;
    }

    let loopBreak = 200;
    let innerLoopBreak = 50 * tags.length;
    // let leftShift = 0;
    do {
      loopBreak--;
      map = Object.create(null);

      // let firstIndex = -1;
      // let lastIndex = -1;
      for (
        let i = leftIndex;
        i <= rightIndex && i <= tags.length && innerLoopBreak-- > 0;
        i++
      ) {
        const el = tags[i];
        if (map[el.tagName] === undefined) {
          map[el.tagName] = 0;
        }
        if (el.open) map[el.tagName] += 1;
        else if (el.close) map[el.tagName] -= 1;
        if (map[el.tagName] < 0) {
          if (leftIndex === 0) {
            console.error(
              'Seems like we have closing tag without matching opening tag. Returning potentially wrong result',
              {
                map,
                resultRange,
              },
            );
            break;
          }
          leftIndex -= 1;
          // leftShift++;
          resultRange.leftShift++;
          // restart the cycle
          map = Object.create(null);
          i = leftIndex - 1;
        }
      }

      // will never happen
      // if (someObjectValueNegative(map))
      // {
      //   if (leftIndex===0){
      //     console.error('Seems like we have closing tag without matching opening tag. Returning potentially wrong result',{map,resultRange});
      //     break;
      //   }
      //
      //   leftIndex-=1;
      //
      //   // resultRange.start = tags[firstIndex-1].index;
      //   // continue;
      // }

      if (someObjectValuePositive(map)) {
        if (rightIndex === tags.length - 1) {
          console.error(
            'Seems like we have opening tag without matching closing tag. Returning potentially wrong result',
            {
              map,
              resultRange,
            },
          );
          break;
        }

        rightIndex += 1;
        // resultRange.end = tags[lastIndex+1].index+tags[lastIndex].tagLength-1;
        // continue;
      }
    } while (!everyObjectValueIsZero(map) && loopBreak > 0);

    resultRange.start = Math.min(tags[leftIndex].index, resultRange.start);
    resultRange.end = Math.max(
      tags[rightIndex].index + tags[rightIndex].tagLength,
      resultRange.end,
    );
    // resultRange.leftShift = leftShift;

    return resultRange;
  }

  /**
   * Возвращает все тэги в строке. Если указано tagName то будут наёдены только теги с указанным названием
   * @param content
   * @param tagName
   */
  public findTagsInString(
    content: string,
    tagName?: string,
  ): RegExpExecArray[] {
    let tags: RegExpExecArray[] = [];
    if (!tagName) {
      tagName = '[a-z0-9]+';
    }
    const regex = new RegExp(
      // `(<\\s*(?<open>${tagName})\\s((?!\\/\\s*>).)*>)|(<\\s*\\/\\s*(?<close>${tagName})\\s*>)|(<\\s*(?<open2>${tagName})\\s*>)|(<\\s*(?<self>${tagName})\\s*[^>]*\\/\\s*>)`,
      // `(<\\s*(?<open>${tagName})\\s[^>]*((?!\\/\\s*>))*>)|(<\\s*\\/\\s*(?<close>${tagName})\\s*>)|(<\\s*(?<open2>${tagName})\\s*>)|(<\\s*(?<self>${tagName})\\s*[^>]*\\/\\s*>)`,
      // `(<\\s*(?<self>${tagName})\\s*[^>]*\\/\\s*>)|(<\\s*(?<open>${tagName})\\s[^>]*>)|(<\\s*\\/\\s*(?<close>${tagName})\\s*>)|(<\\s*(?<open2>${tagName})\\s*>)`,
      `(<\\s*(${tagName})\\s*[^>]*\\/\\s*>)|(<\\s*(${tagName})\\s[^>]*>)|(<\\s*\\/\\s*(${tagName})\\s*>)|(<\\s*(${tagName})\\s*>)`,
      'gmi',
    );

    let m: RegExpExecArray;

    while ((m = regex.exec(content)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      const [, , self, , open, , close, , open2] = m;
      m.groups = {
        self,
        open,
        close,
        open2,
      };

      tags.push(m);
    }
    return tags;
  }
}
