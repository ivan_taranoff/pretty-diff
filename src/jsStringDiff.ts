import * as diff from 'fast-diff';

/**
 * @typedef {string} DiffOperation
 **/

/**
 * @enum {DiffOperation}
 */
enum DiffOperation {
  INSERT = 'insert',
  DELETE = 'delete',
  REPLACE = 'replace',
  MIXED = 'mixed',
}

/**
 * @typedef {Object} DiffResultElement
 * @property {DiffOperation} operation
 * @property {string} content
 * @property {number} start
 * @property {number} length
 */
interface DiffResultElement {
  operation: DiffOperation;
  content: string;
  start: number;
  reference: number;
  length: number;
  contentDeleted?: string; // only applicable to REPLACE
  lengthDeleted?: number; // only applicable to REPLACE
  // cumulativeShift: number;
}

/**
 * Finds difference between two strings and returns difference that can be applied to the oldString to re-create newString
 * @param {string} newString
 * @param {string} oldString
 * @returns {object:{}}
 */
function performDiff(
  newString: string,
  oldString: string,
): DiffResultElement[] {
  let diffResult: diff.Diff[] = diff(oldString, newString);
  // console.log(diffResult);
  let result: DiffResultElement[] = [];
  let currentOldIndex: number = 0;
  let currentNewIndex: number = 0;
  // let cumulativeShift = 0;
  for (let i = 0; i < diffResult.length; i++) {
    let element: diff.Diff = diffResult[i];
    switch (element[0]) {
      case -1:
        //delete

        result.push({
          operation: DiffOperation.DELETE,
          content: element[1],
          start: currentOldIndex,
          reference: currentNewIndex,
          length: element[1].length,
        });
        currentNewIndex += element[1].length;
        // cumulativeShift -= element[1].length;

        break;
      case 0:
        //similar
        currentOldIndex += element[1].length;
        currentNewIndex += element[1].length;
        // cumulativeShift += element[1].length;

        break;
      case 1:
        //insert

        result.push({
          operation: DiffOperation.INSERT,
          content: element[1],
          start: currentOldIndex,
          reference: currentNewIndex,
          length: element[1].length,
          // cumulativeShift
        });
        // cumulativeShift+=element[1].length;

        currentOldIndex += element[1].length;
        break;
      default:
        console.error('unexpected diff result:', element);
        break;
    }
  }

  if (newString.length>oldString.length){

  }
  return result;
}

function performDiffCompact(
  newString: string,
  oldString: string,
): DiffResultElement[] {
  throw new Error('Not implemented');
  let diffResult = performDiff(newString, oldString);
  let result: DiffResultElement[] = [];
  for (let i = 0; i < diffResult.length; i++) {
    if (
      diffResult[i].operation === DiffOperation.DELETE &&
      diffResult[i + 1] &&
      diffResult[i + 1].operation === DiffOperation.INSERT
    ) {
      result.push({
        operation: DiffOperation.REPLACE,
        start: diffResult[i + 1].start,
        reference: diffResult[i + 1].reference,
        length: diffResult[i + 1].length,
        content: diffResult[i + 1].content,
        contentDeleted: diffResult[i].content,
        lengthDeleted: diffResult[i].length,
      });
      diffResult.splice(i, 1);
    } else {
      result.push(diffResult[i]);
    }
  }
  return result;
}

/**
 *
 * @param string
 * @param diffInfo
 */
function applyDiff(string: string, diffInfo: DiffResultElement[]) {
  let diff: DiffResultElement[] = [...diffInfo];
  let result: string = string;

  while (diff.length) {
    // Important note - apply diff changes should be done in the same order as they are defined in an array.
    // while revert should be done in reverse order
    const element: DiffResultElement = diff.shift();

    switch (element.operation) {
      case DiffOperation.DELETE:
        result =
          result.substr(0, element.start) +
          result.substr(element.start + element.length);
        break;

      case DiffOperation.INSERT:
        result =
          result.substr(0, element.start) +
          element.content +
          result.substr(element.start);
        break;
    }
  }
  return result;
}

function revertDiff(string: string, diffInfo: DiffResultElement[]) {
  let diff: DiffResultElement[] = [...diffInfo];
  let result: string = string;

  while (diff.length) {
    // See note in applyDiff
    const element: DiffResultElement = diff.pop();

    switch (element.operation) {
      case DiffOperation.INSERT:
        result =
          result.substr(0, element.start) +
          result.substr(element.start + element.length);
        break;

      case DiffOperation.DELETE:
        result =
          result.substr(0, element.start) +
          element.content +
          result.substr(element.start);
        break;
    }
  }
  return result;
}

export {
  DiffOperation,
  DiffResultElement,
  performDiff,
  performDiffCompact,
  applyDiff,
  revertDiff,
};
