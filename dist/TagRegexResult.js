export class TagRegexResult {
    constructor(tag, index, other) {
        this.tag = tag;
        this.index = index;
        if (other) {
            this.tagName = other.tagName || '';
            this.open = other.open || false;
            this.close = other.close || false;
            this.self = other.self || false;
        }
    }
    static FROM_REGEX_EXEC_ARRAY(input, shiftIndexBy = 0) {
        let result = {
            tagName: '',
            tag: input[0],
            open: false,
            close: false,
            self: false,
            index: input.index + shiftIndexBy,
            tagLength: input[0].length
        };
        if (input.groups) {
            if (input.groups['open'] || input.groups['open2']) {
                result.tagName = input.groups['open'] || input.groups['open2'];
                result.open = true;
            }
            else if (input.groups['self']) {
                result.tagName = input.groups['self'];
                result.self = true;
            }
            else if (input.groups['close']) {
                result.tagName = input.groups['close'];
                result.close = true;
            }
        }
        return result;
    }
}
//# sourceMappingURL=TagRegexResult.js.map