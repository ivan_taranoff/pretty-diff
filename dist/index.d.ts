import { DiffOperation, DiffResultElement } from './jsStringDiff';
import { ITagRegexResult } from './ITagRegexResult';
export interface IAffectedArea {
    oldStart: number;
    newStart: number;
    oldEnd: number;
    newEnd: number;
    diffArr: DiffResultElement[];
    operation: DiffOperation;
}
export interface IRange {
    start: number;
    end: number;
}
export interface IPrettyDiffResult {
    prevContent: string;
    newContent: string;
}
export interface IPrettyDiffOptions {
    ignoreWhitespace: boolean;
    onlyBody: boolean;
}
export declare class PrettyDiff {
    fileNewContents: string;
    filePreviousContents: string;
    private previousTags;
    private newTags;
    private defaultOptions;
    private readonly options;
    constructor(options?: Partial<IPrettyDiffOptions>, newContent?: string, oldContent?: string);
    process(prev?: string, next?: string): {
        prevContent: string;
        newContent: string;
    };
    private cutDoctype;
    insertModTags(diffAreas: IAffectedArea[], prev?: string, next?: string): IPrettyDiffResult;
    calculateAffectedAreas(diff: DiffResultElement[]): IAffectedArea[];
    collapseAreas(areas: IAffectedArea[]): IAffectedArea[];
    calculatePreviousFilePositions(diff: DiffResultElement[]): number[];
    findAffectedArea(diff: DiffResultElement): IAffectedArea | null;
    processInsideOfTagCase(tags: ITagRegexResult[], tagIndex: number): IRange;
    isInsideOfTag(contents: string, startPos: number): boolean;
    diffContainsTags(diff: DiffResultElement): boolean;
    findClosingTagLastIndex(tags: ITagRegexResult[], startIndex: number): number;
    findOpeningTagFirstIndex(tags: ITagRegexResult[], startIndex: number): number;
    private static findMatchingTag;
    findOpeningTagIndex(tags: ITagRegexResult[], startIndex: number): number;
    findCompleteTagRange(tags: ITagRegexResult[], initialRange: IRange): IRange & {
        leftShift: number;
    };
    findTagsInString(content: string, tagName?: string): RegExpExecArray[];
}
