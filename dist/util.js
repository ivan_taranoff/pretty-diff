export function findBackwardIndexOf(heap, needle, start) {
    return heap.slice(0, start).lastIndexOf(needle);
}
export function findForwardIndexOf(heap, needle, start) {
    const result = heap.slice(start + 1).indexOf(needle);
    return ~result ? result + start + 1 : -1;
}
export function findLastIndex(heap, predicate) {
    const index = heap.slice().reverse().findIndex(predicate);
    const count = heap.length - 1;
    return index >= 0 ? count - index : index;
}
export function removeWhitespace(input) {
    return input
        .replace(/([ \t][ \t]+)/g, ' ')
        .replace(/^[ \t]*/, '');
}
export function adjustDiffResults(diffResults, oldContent) {
    return diffResults.map((e) => {
        if (e.content && e.content.length > 0) {
            if (e.content[e.content.length - 1] === '<' &&
                oldContent[e.reference - 1] === '<') {
                return Object.assign(Object.assign({}, e), { content: '<' + e.content.slice(0, -1), start: e.start - 1, reference: e.reference - 1 });
            }
            else if (e.content[0] === '>' &&
                oldContent[e.reference + e.content.length] === '>') {
                return Object.assign(Object.assign({}, e), { content: e.content.slice(1) + '>', start: e.start + 1, reference: e.reference + 1 });
            }
            else
                return e;
        }
        else
            return e;
    });
}
export function everyMapValuesIsZero(input) {
    for (let v of input.values()) {
        if (v !== 0) {
            return false;
        }
    }
    return true;
}
export function everyObjectValueIsZero(input) {
    let keys = Object.keys(input);
    for (let k of keys) {
        if (input[k] !== 0) {
            return false;
        }
    }
    return true;
}
export function someObjectValuePositive(input) {
    let keys = Object.keys(input);
    for (let k of keys) {
        if (input[k] > 0) {
            return true;
        }
    }
    return false;
}
export function someObjectValueNegative(input) {
    let keys = Object.keys(input);
    for (let k of keys) {
        if (input[k] < 0) {
            return true;
        }
    }
    return false;
}
//# sourceMappingURL=util.js.map