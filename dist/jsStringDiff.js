import * as diff from 'fast-diff';
var DiffOperation;
(function (DiffOperation) {
    DiffOperation["INSERT"] = "insert";
    DiffOperation["DELETE"] = "delete";
    DiffOperation["REPLACE"] = "replace";
    DiffOperation["MIXED"] = "mixed";
})(DiffOperation || (DiffOperation = {}));
function performDiff(newString, oldString) {
    let diffResult = diff(oldString, newString);
    let result = [];
    let currentOldIndex = 0;
    let currentNewIndex = 0;
    for (let i = 0; i < diffResult.length; i++) {
        let element = diffResult[i];
        switch (element[0]) {
            case -1:
                result.push({
                    operation: DiffOperation.DELETE,
                    content: element[1],
                    start: currentOldIndex,
                    reference: currentNewIndex,
                    length: element[1].length,
                });
                currentNewIndex += element[1].length;
                break;
            case 0:
                currentOldIndex += element[1].length;
                currentNewIndex += element[1].length;
                break;
            case 1:
                result.push({
                    operation: DiffOperation.INSERT,
                    content: element[1],
                    start: currentOldIndex,
                    reference: currentNewIndex,
                    length: element[1].length,
                });
                currentOldIndex += element[1].length;
                break;
            default:
                console.error('unexpected diff result:', element);
                break;
        }
    }
    if (newString.length > oldString.length) {
    }
    return result;
}
function performDiffCompact(newString, oldString) {
    throw new Error('Not implemented');
    let diffResult = performDiff(newString, oldString);
    let result = [];
    for (let i = 0; i < diffResult.length; i++) {
        if (diffResult[i].operation === DiffOperation.DELETE &&
            diffResult[i + 1] &&
            diffResult[i + 1].operation === DiffOperation.INSERT) {
            result.push({
                operation: DiffOperation.REPLACE,
                start: diffResult[i + 1].start,
                reference: diffResult[i + 1].reference,
                length: diffResult[i + 1].length,
                content: diffResult[i + 1].content,
                contentDeleted: diffResult[i].content,
                lengthDeleted: diffResult[i].length,
            });
            diffResult.splice(i, 1);
        }
        else {
            result.push(diffResult[i]);
        }
    }
    return result;
}
function applyDiff(string, diffInfo) {
    let diff = [...diffInfo];
    let result = string;
    while (diff.length) {
        const element = diff.shift();
        switch (element.operation) {
            case DiffOperation.DELETE:
                result =
                    result.substr(0, element.start) +
                        result.substr(element.start + element.length);
                break;
            case DiffOperation.INSERT:
                result =
                    result.substr(0, element.start) +
                        element.content +
                        result.substr(element.start);
                break;
        }
    }
    return result;
}
function revertDiff(string, diffInfo) {
    let diff = [...diffInfo];
    let result = string;
    while (diff.length) {
        const element = diff.pop();
        switch (element.operation) {
            case DiffOperation.INSERT:
                result =
                    result.substr(0, element.start) +
                        result.substr(element.start + element.length);
                break;
            case DiffOperation.DELETE:
                result =
                    result.substr(0, element.start) +
                        element.content +
                        result.substr(element.start);
                break;
        }
    }
    return result;
}
export { DiffOperation, performDiff, performDiffCompact, applyDiff, revertDiff, };
//# sourceMappingURL=jsStringDiff.js.map