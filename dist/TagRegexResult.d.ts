import { ITagRegexResult } from './ITagRegexResult';
export declare class TagRegexResult implements ITagRegexResult {
    close: boolean;
    index: number;
    open: boolean;
    self: boolean;
    tag: string;
    tagName: string;
    tagLength: number;
    constructor(tag: string, index: number, other?: Partial<ITagRegexResult>);
    static FROM_REGEX_EXEC_ARRAY(input: RegExpExecArray, shiftIndexBy?: number): ITagRegexResult;
}
