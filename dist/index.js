import { DiffOperation, performDiff } from './jsStringDiff';
import { adjustDiffResults, everyObjectValueIsZero, findBackwardIndexOf, findForwardIndexOf, findLastIndex, removeWhitespace, someObjectValuePositive, } from './util';
import { TagRegexResult } from './TagRegexResult';
var TagType;
(function (TagType) {
    TagType["INSERT_START"] = "<ins>";
    TagType["INSERT_END"] = "</ins>";
    TagType["DELETE_START"] = "<del>";
    TagType["DELETE_END"] = "</del>";
    TagType["REPLACE_START"] = "<repl>";
    TagType["REPLACE_END"] = "</repl>";
})(TagType || (TagType = {}));
export class PrettyDiff {
    constructor(options, newContent = '', oldContent = '') {
        this.defaultOptions = {
            ignoreWhitespace: false,
            onlyBody: true,
        };
        this.options = Object.assign(Object.assign({}, this.defaultOptions), options);
        this.fileNewContents = newContent;
        this.filePreviousContents = oldContent;
    }
    process(prev = this.filePreviousContents, next = this.fileNewContents) {
        let prevDoctype, newDoctype, originalPrev, originalNext, xhtmlPrev, xhtmlNext;
        originalPrev = prev;
        originalNext = next;
        if (window !== undefined) {
            const parser = new DOMParser();
            const serializer = new XMLSerializer();
            try {
                const docPrev = parser.parseFromString(originalPrev, 'text/html');
                const docNext = parser.parseFromString(originalNext, 'text/html');
                xhtmlPrev = serializer.serializeToString(docPrev);
                xhtmlNext = serializer.serializeToString(docNext);
            }
            catch (e) {
                console.error(e);
                xhtmlPrev = prev;
                xhtmlNext = next;
            }
        }
        else {
            xhtmlPrev = originalPrev;
            xhtmlNext = originalNext;
        }
        prevDoctype = this.cutDoctype(xhtmlPrev);
        newDoctype = this.cutDoctype(xhtmlNext);
        this.filePreviousContents = prevDoctype.content;
        this.fileNewContents = newDoctype.content;
        let prevBeforeBody = '', prevAfterBody = '', newBeforeBody = '', newAfterBody = '';
        if (this.options.onlyBody) {
            const regex = /^(.*<\s*body[^>]*>)(.*)(<\s*\/\s*body\s*>.*)$/s;
            let matches = regex.exec(this.filePreviousContents);
            if (matches) {
                [, prevBeforeBody, this.filePreviousContents, prevAfterBody] = matches;
            }
            matches = regex.exec(this.fileNewContents);
            if (matches) {
                [, newBeforeBody, this.fileNewContents, newAfterBody] = matches;
            }
        }
        if (this.options.ignoreWhitespace) {
            this.filePreviousContents = removeWhitespace(this.filePreviousContents);
            this.fileNewContents = removeWhitespace(this.filePreviousContents);
        }
        const diff = performDiff(this.fileNewContents, this.filePreviousContents);
        const diffFixed = adjustDiffResults(diff, this.filePreviousContents);
        this.previousTags = this.findTagsInString(this.filePreviousContents).map((e) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
        this.newTags = this.findTagsInString(this.fileNewContents).map((e) => TagRegexResult.FROM_REGEX_EXEC_ARRAY(e));
        let diffAreas = this.calculateAffectedAreas(diffFixed);
        let diffAreasCollapsed = this.collapseAreas(diffAreas);
        let result = this.insertModTags(diffAreasCollapsed);
        if (this.options.onlyBody) {
            result.prevContent = prevBeforeBody + result.prevContent + prevAfterBody;
            result.newContent = newBeforeBody + result.newContent + newAfterBody;
        }
        result.prevContent = prevDoctype.doctype + result.prevContent;
        result.newContent = newDoctype.doctype + result.newContent;
        return result;
    }
    cutDoctype(input) {
        const regex = /^(.*<\s*!\s*DOCTYPE[^>]*>)/s;
        const result = regex.exec(input);
        return {
            content: input.replace(regex, ''),
            doctype: (result && result[0]) || '',
        };
    }
    insertModTags(diffAreas, prev = this.filePreviousContents, next = this.fileNewContents) {
        let prevContent = prev;
        let newContent = next;
        let sortedByOldEnd = diffAreas.sort((a, b) => b.oldEnd - a.oldEnd);
        let sortedByNewEnd = diffAreas.sort((a, b) => b.newEnd - a.newEnd);
        const perform = (array, content, prop) => {
            return array
                .flatMap((e) => {
                let tagStart, tagEnd;
                switch (e.operation) {
                    case DiffOperation.DELETE:
                        tagStart = TagType.DELETE_START;
                        tagEnd = TagType.DELETE_END;
                        break;
                    case DiffOperation.INSERT:
                        tagStart =
                            prop === 'old' ? TagType.DELETE_START : TagType.INSERT_START;
                        tagEnd = prop === 'old' ? TagType.DELETE_END : TagType.INSERT_END;
                        break;
                    case DiffOperation.MIXED:
                    case DiffOperation.REPLACE:
                        tagStart = TagType.REPLACE_START;
                        tagEnd = TagType.REPLACE_END;
                        break;
                }
                return [
                    { start: e[prop + 'End'], tagString: tagEnd },
                    { start: e[prop + 'Start'], tagString: tagStart },
                ];
            })
                .reduce((acc, el) => {
                return (acc.substring(0, el.start) + el.tagString + acc.substring(el.start));
            }, content);
        };
        prevContent = perform(sortedByOldEnd, prevContent, 'old');
        newContent = perform(sortedByNewEnd, newContent, 'new');
        return { prevContent, newContent };
    }
    calculateAffectedAreas(diff) {
        return diff.map((e) => {
            return this.findAffectedArea(e);
        });
    }
    collapseAreas(areas) {
        let result = [...areas];
        for (let i = 1; i < result.length; i++) {
            for (let c = 0; c < i && c < result.length; c++) {
                if (intersects(result[c], result[i])) {
                    result[c] = getIntersectedElement(result[c], result[i]);
                    result.splice(i, 1);
                    i--;
                }
            }
        }
        return result.sort((a, b) => a.newStart - b.newStart);
        function intersects(a, b) {
            let ANinBN = a.newEnd <= b.newEnd && a.newStart >= b.newStart;
            let BNinAN = b.newEnd <= a.newEnd && b.newStart >= a.newStart;
            let AOinBO = a.oldEnd <= b.oldEnd && a.oldStart >= b.oldStart;
            let BOinAO = b.oldEnd <= a.oldEnd && b.oldStart >= a.oldStart;
            let ANEinBN = a.newEnd <= b.newEnd && a.newEnd >= b.newStart;
            let ANSinBN = a.newStart <= b.newEnd && a.newStart >= b.newStart;
            let AOEinBO = a.oldEnd <= b.oldEnd && a.oldEnd >= b.oldStart;
            let AOSinBO = a.oldStart <= b.oldEnd && a.oldStart >= b.oldStart;
            return (ANinBN ||
                BNinAN ||
                AOinBO ||
                BOinAO ||
                ANEinBN ||
                ANSinBN ||
                AOEinBO ||
                AOSinBO);
        }
        function getIntersectedElement(a, b) {
            let acc = Object.assign({}, a);
            acc.newEnd = Math.max(acc.newEnd, b.newEnd);
            acc.newStart = Math.min(acc.newStart, b.newStart);
            acc.oldEnd = Math.max(acc.oldEnd, b.oldEnd);
            acc.oldStart = Math.min(acc.oldStart, b.oldStart);
            if (acc.operation !== b.operation) {
                acc.operation = DiffOperation.MIXED;
            }
            acc.diffArr = [...acc.diffArr, ...b.diffArr];
            return acc;
        }
    }
    calculatePreviousFilePositions(diff) {
        return diff.map((e, i, a) => {
            return a
                .slice(0, i)
                .reduce((acc, diffItem) => {
                if (diffItem.operation === DiffOperation.DELETE)
                    acc -= diffItem.length;
                else if (diffItem.operation === DiffOperation.INSERT)
                    acc += diffItem.length;
                else if (diffItem.operation === DiffOperation.REPLACE)
                    acc += diffItem.length - diffItem.lengthDeleted;
                return acc;
            }, e.start);
        });
    }
    findAffectedArea(diff) {
        let insideOfTagIndexPrev = this.previousTags.findIndex((e) => e.index < diff.reference && e.index + e.tagLength > diff.reference);
        let insideOfTagIndexNew = this.newTags.findIndex((e) => e.index < diff.start && e.index + e.tagLength > diff.start);
        if (~insideOfTagIndexPrev || ~insideOfTagIndexNew) {
            let result = {
                oldStart: diff.reference,
                oldEnd: diff.reference,
                newStart: diff.start,
                newEnd: diff.start,
                operation: diff.operation,
                diffArr: [diff],
            };
            if (diff.operation === DiffOperation.INSERT) {
                result.newEnd += diff.length;
            }
            else if (diff.operation === DiffOperation.DELETE) {
                result.oldEnd += diff.length;
            }
            if (~insideOfTagIndexNew) {
                let newContentAffectedRange = this.findCompleteTagRange(this.newTags, {
                    start: result.newStart,
                    end: result.newEnd,
                });
                result.newStart = Math.min(newContentAffectedRange.start, result.newStart);
                result.newEnd = Math.max(newContentAffectedRange.end, result.newEnd);
            }
            if (~insideOfTagIndexPrev) {
                let prevContentAffectedRange = this.findCompleteTagRange(this.previousTags, {
                    start: result.oldStart,
                    end: result.oldEnd,
                });
                result.oldStart = Math.min(prevContentAffectedRange.start, result.oldStart);
                result.oldEnd = Math.max(prevContentAffectedRange.end, result.oldEnd);
            }
            return result;
        }
        else if (this.diffContainsTags(diff)) {
            let result = {
                oldStart: diff.reference,
                oldEnd: diff.reference,
                newStart: diff.start,
                newEnd: diff.start,
                operation: diff.operation,
                diffArr: [diff],
            };
            if (diff.operation === DiffOperation.INSERT) {
                result.newEnd += diff.length;
                let range = this.findCompleteTagRange(this.newTags, {
                    start: result.newStart,
                    end: result.newEnd,
                });
                result.newStart = Math.min(range.start, diff.start);
                result.newEnd = Math.max(range.end, diff.start + diff.length);
            }
            if (diff.operation === DiffOperation.DELETE) {
                result.oldEnd += diff.length;
                let range = this.findCompleteTagRange(this.previousTags, {
                    start: result.oldStart,
                    end: result.oldEnd,
                });
                if (range.leftShift > 0) {
                    let tagIndexFollowingStart = this.newTags.findIndex((e) => e.index + e.tagLength > result.newStart);
                    if (tagIndexFollowingStart - range.leftShift < 0) {
                        console.error('Found leftShift is too big, results in going below 0 index,', tagIndexFollowingStart - range.leftShift);
                        return result;
                    }
                    const newRange = { start: this.newTags[tagIndexFollowingStart - range.leftShift].index, end: result.newEnd };
                    let newRangeExtended = this.findCompleteTagRange(this.newTags, newRange);
                    result.newStart = Math.min(newRangeExtended.start, result.newStart);
                    result.newEnd = Math.max(newRangeExtended.end, result.newEnd);
                    result.operation = DiffOperation.REPLACE;
                }
                result.oldStart = Math.min(range.start, diff.reference);
                result.oldEnd = Math.max(range.end, diff.reference + diff.length);
            }
            return result;
        }
        else {
            if (diff.operation === DiffOperation.INSERT) {
                return {
                    oldStart: diff.reference,
                    oldEnd: diff.reference,
                    newStart: diff.start,
                    newEnd: diff.start + diff.length,
                    operation: diff.operation,
                    diffArr: [diff],
                };
            }
            else if (diff.operation === DiffOperation.DELETE) {
                return {
                    oldStart: diff.reference,
                    oldEnd: diff.reference + diff.length,
                    newStart: diff.start,
                    newEnd: diff.start,
                    operation: diff.operation,
                    diffArr: [diff],
                };
            }
        }
        return null;
    }
    processInsideOfTagCase(tags, tagIndex) {
        const tag = tags[tagIndex];
        let result = {
            start: tag.index,
            end: tag.index + tag.tagLength,
        };
        if (tag.self) {
            result.start = tag.index;
            result.end = tag.index + tag.tagLength;
        }
        else if (tag.open) {
            let closingTagIndex = this.findClosingTagLastIndex(tags, tagIndex);
            if (~closingTagIndex) {
                result.start = tag.index;
                result.end = closingTagIndex;
            }
            else {
                console.error('ERROR 106 - no matching tag found for ', tag);
            }
        }
        else if (tag.close) {
            let indexShift = this.findOpeningTagFirstIndex(tags, tagIndex);
            if (~indexShift) {
                result.start = indexShift;
                result.end = tag.index + tag.tagLength;
            }
            else {
                console.error('ERROR 108 - no matching tag found for ', tag);
            }
        }
        return result;
    }
    isInsideOfTag(contents, startPos) {
        if (contents[startPos] === '>')
            return true;
        const fwdL = findForwardIndexOf(contents, '>', startPos);
        const fwdR = findForwardIndexOf(contents, '<', startPos);
        const bwdL = findBackwardIndexOf(contents, '>', startPos);
        const bwdR = findBackwardIndexOf(contents, '<', startPos);
        return !!(~fwdL && ~bwdR && (fwdL < fwdR || !~fwdR) && bwdR > bwdL);
    }
    diffContainsTags(diff) {
        return /[><]/gi.test(diff.content);
    }
    findClosingTagLastIndex(tags, startIndex) {
        const tag = tags[startIndex];
        let result = PrettyDiff.findMatchingTag(tags
            .slice(startIndex)
            .filter((e) => e.tagName === tag.tagName));
        if (result !== null) {
            return result.index + result.tagLength;
        }
        return -1;
    }
    findOpeningTagFirstIndex(tags, startIndex) {
        const tag = tags[startIndex];
        let result = PrettyDiff.findMatchingTag(tags
            .slice(0, startIndex + 1)
            .filter((e) => e.tagName === tag.tagName)
            .reverse());
        if (result !== null) {
            return result.index;
        }
        return -1;
    }
    static findMatchingTag(tags) {
        let currentTagCount = 0;
        for (let i = 0; i < tags.length; i++) {
            if (tags[i].open) {
                currentTagCount++;
            }
            else if (tags[i].close) {
                currentTagCount--;
            }
            if (currentTagCount === 0) {
                return tags[i];
            }
        }
        return null;
    }
    findOpeningTagIndex(tags, startIndex) {
        const search = tags[startIndex];
        if (!search.close) {
            return -1;
        }
        let currentTagCount = 0;
        for (let i = startIndex; i >= 0; i--) {
            if (tags[i].tagName !== search.tagName)
                continue;
            if (tags[i].open) {
                currentTagCount++;
            }
            else if (tags[i].close) {
                currentTagCount--;
            }
            if (currentTagCount === 0) {
                return i;
            }
        }
        return -1;
    }
    findCompleteTagRange(tags, initialRange) {
        let resultRange = Object.assign(Object.assign({}, initialRange), { leftShift: 0 });
        let currentIndex = tags.findIndex((e) => e.index + e.tagLength > resultRange.start);
        if (resultRange.start > tags[currentIndex].index)
            resultRange.start = tags[currentIndex].index;
        if (resultRange.end <
            tags[currentIndex].index + tags[currentIndex].tagLength) {
            resultRange.end = tags[currentIndex].index + tags[currentIndex].tagLength;
        }
        let selfClosingCheck = tags
            .filter((e) => e.index + e.tagLength > resultRange.start &&
            e.index < resultRange.end)
            .map((e) => {
            if (resultRange.start > e.index)
                resultRange.start = e.index;
            if (resultRange.end < e.index + e.tagLength) {
                resultRange.end = e.index + e.tagLength;
            }
            return e;
        })
            .reduce((a, e) => a && e.self, true);
        if (selfClosingCheck)
            return resultRange;
        let map = Object.create(null);
        let leftIndex = tags.findIndex((e) => e.index + e.tagLength > resultRange.start);
        let rightIndex = findLastIndex(tags, (e) => e.index < resultRange.end);
        if (!~leftIndex || !~rightIndex) {
            console.error('Unable to find suitable tag', { resultRange, tags });
            return resultRange;
        }
        let loopBreak = 200;
        let innerLoopBreak = 50 * tags.length;
        do {
            loopBreak--;
            map = Object.create(null);
            for (let i = leftIndex; i <= rightIndex && i <= tags.length && innerLoopBreak-- > 0; i++) {
                const el = tags[i];
                if (map[el.tagName] === undefined) {
                    map[el.tagName] = 0;
                }
                if (el.open)
                    map[el.tagName] += 1;
                else if (el.close)
                    map[el.tagName] -= 1;
                if (map[el.tagName] < 0) {
                    if (leftIndex === 0) {
                        console.error('Seems like we have closing tag without matching opening tag. Returning potentially wrong result', { map, resultRange });
                        break;
                    }
                    leftIndex -= 1;
                    resultRange.leftShift++;
                    map = Object.create(null);
                    i = leftIndex - 1;
                }
            }
            if (someObjectValuePositive(map)) {
                if (rightIndex === tags.length - 1) {
                    console.error('Seems like we have opening tag without matching closing tag. Returning potentially wrong result', { map, resultRange });
                    break;
                }
                rightIndex += 1;
            }
        } while (!everyObjectValueIsZero(map) && loopBreak > 0);
        resultRange.start = Math.min(tags[leftIndex].index, resultRange.start);
        resultRange.end = Math.max(tags[rightIndex].index + tags[rightIndex].tagLength, resultRange.end);
        return resultRange;
    }
    findTagsInString(content, tagName) {
        let tags = [];
        if (!tagName) {
            tagName = '[a-z0-9]+';
        }
        const regex = new RegExp(`(<\\s*(${tagName})\\s*[^>]*\\/\\s*>)|(<\\s*(${tagName})\\s[^>]*>)|(<\\s*\\/\\s*(${tagName})\\s*>)|(<\\s*(${tagName})\\s*>)`, 'gmi');
        let m;
        while ((m = regex.exec(content)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            const [, , self, , open, , close, , open2] = m;
            m.groups = {
                self,
                open,
                close,
                open2
            };
            tags.push(m);
        }
        return tags;
    }
}
//# sourceMappingURL=index.js.map