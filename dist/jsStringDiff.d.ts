declare enum DiffOperation {
    INSERT = "insert",
    DELETE = "delete",
    REPLACE = "replace",
    MIXED = "mixed"
}
interface DiffResultElement {
    operation: DiffOperation;
    content: string;
    start: number;
    reference: number;
    length: number;
    contentDeleted?: string;
    lengthDeleted?: number;
}
declare function performDiff(newString: string, oldString: string): DiffResultElement[];
declare function performDiffCompact(newString: string, oldString: string): DiffResultElement[];
declare function applyDiff(string: string, diffInfo: DiffResultElement[]): string;
declare function revertDiff(string: string, diffInfo: DiffResultElement[]): string;
export { DiffOperation, DiffResultElement, performDiff, performDiffCompact, applyDiff, revertDiff, };
