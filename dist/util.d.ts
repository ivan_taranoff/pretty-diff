import { DiffResultElement } from './jsStringDiff';
export declare function findBackwardIndexOf(heap: string, needle: string, start: number): number;
export declare function findForwardIndexOf(heap: string, needle: string, start: number): number;
export declare function findLastIndex<T>(heap: T[], predicate: (e: T) => boolean): number;
export declare function removeWhitespace(input: string): string;
export declare function adjustDiffResults(diffResults: DiffResultElement[], oldContent: string): DiffResultElement[];
export declare function everyMapValuesIsZero(input: Map<string, number>): boolean;
export declare function everyObjectValueIsZero(input: {
    [key: string]: number;
}): boolean;
export declare function someObjectValuePositive(input: {
    [key: string]: number;
}): boolean;
export declare function someObjectValueNegative(input: {
    [key: string]: number;
}): boolean;
